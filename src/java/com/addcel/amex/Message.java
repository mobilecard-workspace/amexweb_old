/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.amex;

import com.addcel.tools.DatabaseImpl;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.naming.InitialContext;

/**
 *
 * @author ADDCEL13
 */
public class Message {
    public static void check(String code, String msg)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        InitialContext ic = null;
        
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();
            int lastIdn = 0;

            //Bloqueando tablas
            query = "LOCK TABLES Mensaje WRITE";
            st.execute(query);            

            //Ejecutando Queries

            //Obteniendo ultimo id
            query = "SELECT ifnull(count(MensajeId),0) FROM Mensaje";
            rs = st.executeQuery(query);
            if(rs.next())
            {
                lastIdn = rs.getInt(1);
            }
            
            if(lastIdn==0)
            {
                //Insertando datos
                query = "INSERT INTO Mensaje (MensajeId, MensajeDsc,MensajeOrigen) VALUES('"+code+"','"+msg+"','Amex')";
                System.out.println("Insert:"+query);
                st.executeUpdate(query);
            }

            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                //Desatorar tablas en caso de que se hayan quedado atoradas
                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
                {
                    st.execute("UNLOCK TABLES");                    
                }
                
                //Statement
                if(st!=null && st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
                ds = new DatabaseImpl();
                conn = ds.getConnection();
                st = conn.createStatement();
                st.execute("UNLOCK TABLES");
                st.close();                    
                conn.close();
               
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
    }
    
}
