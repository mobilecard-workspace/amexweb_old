package com.addcel.amex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.common.MCUser;
import com.addcel.tools.DatabaseImpl;

/**
 *
 * @author ADDCEL13
 */
public class TransactionControl {
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionControl.class);
    
    //static int transactionId=0;
    
    public static void updateTransaction(int transactionId,  AmexResponse response)
    {
        TransactionControl.updateTransaction(transactionId,  response,"");
    }
    
    
    public static void updateTransaction(int transactionId,  AmexResponse response, String accReference)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
//        ResultSet rs = null;
        String query = "";
//        int lastError = 0;
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Actualizando mensaje de AMex
            query = "UPDATE Transaccion SET MensajeId = '"+response.codigo + "', "
                    + " transaccionAccReference = '"+accReference+"', "
                    + " transaccionApprovalCode = '"+response.getApprovalCode()+"' "
                    + " WHERE transaccionIdn="+transactionId;
            logger.debug("Update:"+query);
            st.executeUpdate(query);
            
//            //Ingresando Errores
//            if(response.hasErrors())
//            {
//                query = "LOCK TABLES Error WRITE";
//                st.execute(query);
//                
//                //Obteniendo ultimo id
//                query = "SELECT ifnull(max(errorIdn),0) FROM Error";
//                rs = st.executeQuery(query);
//                if(rs.next())
//                {
//                    lastError = rs.getInt(1);
//                }
//                
//                List errores = response.getErrores();
//                
//                for(int i=0;i<errores.size();i++)
//                {
//                    //Actualizando mensaje de AMex}
//                    AmexResponse error = (AmexResponse)errores.get(i);
//                    query = "INSERT INTO Errores (ErrorIdn, ErrorCodigo ,ErrorDsc,transaccionIdn)"
//                            + " VALUES ("+lastError+",'"+error.codigo+"','"+error.mensaje+"',"+transactionId+")";
//                    logger.debug("Insert:"+query);
//                    st.executeUpdate(query);
//                    lastError++;
//                }
//            }
        }
        catch(Exception e)
        {
            logger.debug("ECOMMERCE POOL: General Exception: ", e);            
        }
        finally
        {
            ds.closeConSt(st, conn);
        }
        
    }
    
    public static void updateReversal(long originalTransactionId, long reversalTransactionId)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        String query = "";

        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Actualizando mensaje de AMex
            query = "UPDATE Transaccion SET transaccionReversal='R', transaccionReversalIdn="+reversalTransactionId
                    +" WHERE transaccionIdn="+originalTransactionId;
            logger.debug("Update:"+query);
            st.executeUpdate(query);
        }
        catch(Exception e)
        {
            logger.debug("ECOMMERCE POOL: General Exception: ", e);            
        }
        finally
        {
            ds.closeConSt(st, conn);
        }
        
    }
    
    
    public static int createTransaction(String localDateTime, String id, String dsc, String usr, String tarjeta, 
            String expira, String monto, String cid, String nombres, String apellidos,
            String direccion, String telefono, String email, String cp, 
            String mensajeId, String transaccionTipo, String transaccionSubTipo, String transaccionAcceptorId)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        String query = "";
        int transactionId=0;
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            transactionId = getSecuencia();

            //Insertando datos
            query = "INSERT INTO Transaccion (transaccionIdn, transaccionId, transaccionLocalDateTime,transaccionDsc,transaccionUsuario,"+
                    "transaccionTarjeta, transaccionExpira, transaccionMonto, transaccionCid, transaccionNombres,"
                    + "transaccionApellidos,transaccionDireccion,transaccionTelefono,transaccionEmail,transaccionCp,"
                    + "transaccionTipo,transaccionSubTipo,MensajeId,transaccionAcceptorId)"+
                    " VALUES "+
                    "(" + transactionId + ",'" + id + "','"+localDateTime+"','" + dsc + "','" + usr + "'" +
                    ",'" + tarjeta + "','" + expira + "','" + monto + "','" + cid+ "','" + nombres + "',"
                    + "'" + apellidos + "','" + direccion + "','" + telefono + "','" + email + "','" + cp + "',"
                    + "'" + transaccionTipo + "','" + transaccionSubTipo + "','" + mensajeId + "','"+transaccionAcceptorId+"')";
            
            logger.debug("Insert:"+query);
            st.executeUpdate(query);
        }
        catch(Exception e)
        {
            logger.debug("ECOMMERCE POOL: General Exception: ", e);            
        }
        finally
        {
        	ds.closeConSt(st, conn);
        }
        return transactionId;
    }
    
    public static AmexData getData(long transactionIdn, String systemTraceAuditNumber)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        AmexData data = new AmexData();
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Obteniendo ultimo id
            query = "SELECT transaccionLocalDateTime,transaccionMonto, transaccionTarjeta,transaccionExpira, transaccionCid,transaccionTipo,"
                    + "transaccionAccReference  FROM Transaccion WHERE transaccionIdn="+transactionIdn;
            rs = st.executeQuery(query);
            if(rs.next())
            {
                data.setLocalTransactionDateAndTime(rs.getString(1)); 
                data.setAmount(rs.getString(2)); 
                data.setPrimaryAccountNumber(rs.getString(3)); 
                data.setCardExpirationDate(rs.getString(4)); 
                data.setCid(rs.getString(5)); 
                data.setMessageType(rs.getString(6)); 
                data.setAccReferenceData(rs.getString(7)); 
                data.setSystemTraceAuditNumber(systemTraceAuditNumber);
            }
        }
        catch(Exception e)
        {
            logger.debug("ECOMMERCE POOL: General Exception: ", e);            
        }
        finally
        {
            ds.closeConPrepStResSet(st, rs, conn);
        }
        return data;
    
    }
    
    public static AmexData getData(long transactionIdn)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        AmexData data = new AmexData();
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Obteniendo ultimo id
            query = "SELECT transaccionLocalDateTime,transaccionMonto, transaccionTarjeta,transaccionExpira, transaccionCid,transaccionTipo,"
                    + "transaccionAccReference  FROM Transaccion WHERE transaccionIdn="+transactionIdn;
            rs = st.executeQuery(query);
            if(rs.next())
            {
                data.setLocalTransactionDateAndTime(rs.getString(1)); 
                data.setAmount(rs.getString(2)); 
                data.setPrimaryAccountNumber(rs.getString(3)); 
                data.setCardExpirationDate(rs.getString(4)); 
                data.setCid(rs.getString(5)); 
                data.setMessageType(rs.getString(6)); 
                data.setAccReferenceData(rs.getString(7)); 
                data.setSystemTraceAuditNumber((transactionIdn+""));
            }
        }
        catch(Exception e)
        {
            logger.debug("ECOMMERCE POOL: General Exception: ", e);            
        }
        finally
        {
            ds.closeConPrepStResSet(st, rs, conn);
        }
        return data;
    
    }
    
//    public static void setBitacora()
//    {
//        DatabaseImpl ds = new DatabaseImpl();
//        Connection conn = null;
//        Statement st = null;
//        ResultSet rs = null;
//        String query = "";
//        InitialContext ic = null;
//        
//        try
//        {
//            conn = ds.getConnection();
//            st = conn.createStatement();
//            int lastIdn = 0;
//
//            //Bloqueando tablas
//            query = "LOCK TABLES Bitacora WRITE";
//            st.execute(query);            
//
//            //Ejecutando Queries
//
//            //Obteniendo ultimo id
//            query = "SELECT ifnull(max(BitacoraIdn),0) FROM Bitacora";
//            rs = st.executeQuery(query);
//            if(rs.next())
//            {
//                lastIdn = rs.getInt(1);
//            }
//
//            lastIdn++;
//
//            //Insertando datos
////            query = "INSERT INTO Bitacora (BitacoraIdn, SistemaIdn, BitacoraTarjeta,BitacoraNoTarjeta,"+
////                    "EstadoId, BitacoraTipo, BitacoraDsc, transaccionIdn)"+
////                    " VALUES "+
////                    "(" +lastIdn + "," + sistema + ",'" + tarjeta + "','" + noTarjeta + "'" +
////                    ",'" + estado + "','" + tipo + "','"+infoAdicional+"',"+transaccion+")";
//            logger.debug("Insert:"+query);
//            st.executeUpdate(query);
//
//
//            //Desloqueando tablas
//            query = "UNLOCK TABLES";
//            st.execute(query);            
//        }
//        catch(Exception e)
//        {
//            
//        }
//        finally
//        {
//           ds.closeConPrepStResSet(st, rs, conn);
//        }
//        
//        //return transactionId;
//    }
    

    public static MCUser getUserData(String userId)
    {
        DatabaseImpl ds = new DatabaseImpl("mobilecard");
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        MCUser mobileCardUser = null;
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Obteniendo ultimo id
            query = "select id_usuario,usr_login,usr_nombre, usr_apellido, usr_materno, usr_tdc_numero, usr_tdc_vigencia, desc_tipo_tarjeta "
                    + "from t_usuarios left join t_tipo_tarjeta on t_usuarios.id_tipo_tarjeta=t_tipo_tarjeta.id_tipo_tarjeta "
                    + "where id_usr_status = 1 and usr_login='"+userId+"'";
            rs = st.executeQuery(query);
            logger.debug(query);
            if(rs.next())
            {
                mobileCardUser =  new MCUser();
                mobileCardUser.setUserId(rs.getString(1));
                mobileCardUser.setUserLogin(rs.getString(2));
                mobileCardUser.setUserNombre(rs.getString(3));
                mobileCardUser.setUserApellido(rs.getString(4));
                //mobileCardUser.setUserId(rs.getString(5)); Apellido materno
                mobileCardUser.setUserTdc(rs.getString(6));
                mobileCardUser.setUserVig(rs.getString(7));
                mobileCardUser.setUserTipoTarjeta(rs.getString(8));
                
            }
        }
        catch(Exception e)
        {
            logger.debug("ECOMMERCE POOL: General Exception: ", e);            
        }
        finally
        {
            ds.closeConPrepStResSet(st, rs, conn);
        }
        return mobileCardUser;
    
    }
    
    public static synchronized int getSecuencia(){
        DatabaseImpl ds = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        int lastId = 0;
        try{
            ds = new DatabaseImpl("mobilecard");
            conn=ds.getConnection();
            pst=conn.prepareStatement("select mobilecard.get_next_value(\"@SECUENCIA_PROSA_AMEX\")");
            rs=pst.executeQuery();
            if(rs.next()){
                lastId=rs.getInt(1);
                logger.debug("SECUENCIA_PROSA: " + lastId);
            }else{
                logger.error("SECUENCIA_PROSA: No se obtubo ninguna secuencia.");
            }
        }catch(Exception ex){
            logger.error("ECOMMERCE POOL: Error interno de base de datos: {}", ex);        
        }finally{
            if(ds != null){
                ds.closeConPrepStResSet(pst, rs, conn);
            }
        }
        return lastId;
    }
    
    public static synchronized int getSecuenciaReverso(){
        DatabaseImpl ds = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        int lastId = 0;
        try{
            ds = new DatabaseImpl("mobilecard");
            conn=ds.getConnection();
            pst=conn.prepareStatement("select mobilecard.get_next_value(\"@SECUENCIA_PROSA_REVERSO\")");
            rs=pst.executeQuery();
            if(rs.next()){
                lastId=rs.getInt(1);
                logger.debug("SECUENCIA_PROSA_REVERSO: " + lastId);
            }else{
                logger.error("SECUENCIA_PROSA_REVERSO: No se obtubo ninguna secuencia.");
            }
        }catch(Exception ex){
            logger.error("ECOMMERCE POOL: Error interno de base de datos: {}", ex);        
        }finally{
            if(ds != null){
                ds.closeConPrepStResSet(pst, rs, conn);
            }
        }
        return lastId;
    }

}
