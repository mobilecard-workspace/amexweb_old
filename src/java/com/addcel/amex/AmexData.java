/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.amex;

/**
 *
 * @author ADDCEL13
 */
public class AmexData {
 
    private String messageType;
    private String processingCode;
    private String primaryAccountNumber;
    private String cardExpirationDate;
    private String systemTraceAuditNumber;
    private String localTransactionDateAndTime;
    private String amount;
    private double numberAmount;
    private String cid;
    private String acceptorId;
    
    private String billingCP;
    private String billingDireccion;
    private String billingNombre;
    private String billingApellido;
    private String billingTel;
    private String billingEmail;
    
    private String shipToCP;
    private String shipToDireccion;
    private String shipToNombre;
    private String shipToApellido;
    private String shipToTel;
    private String shipToEmail;
    
    private String serviceIdentifier;
    private String typeIdentifier;
    
    private String actionCode;
    private String actionDsc;
    private String accReferenceData;
    private String approvalCode;
    
    private String paymentType;
    private String installations;
    private String dpp;
    

    @Override
    public String toString() {
        return 
                new StringBuilder().append(
                "systemTraceAuditNumber=" ).append( systemTraceAuditNumber ).append( "\n" ).append( 
                "processingCode=" ).append( processingCode ).append( "\n" ).append( 
                "messageType=" ).append( messageType ).append( "\n" ).append( 
                "primaryAccountNumber=" ).append( primaryAccountNumber ).append( "\n" ).append( 
                "cardExpirationDate=" ).append( cardExpirationDate ).append( "\n" ).append( 
                "localTransactionDateAndTime=" ).append( localTransactionDateAndTime ).append( "\n" ).append( 
                "amount=" ).append( amount ).append( "\n" ).append( 
                "cid=" ).append( cid ).append( "\n\n" ).append( 
                "billingCP=" ).append( billingCP ).append( "\n" ).append( 
                "billingDireccion=" ).append( billingDireccion ).append( "\n" ).append( 
                "billingNombre=" ).append( billingNombre ).append( "\n" ).append( 
                "billingApellido=" ).append( billingApellido ).append( "\n" ).append( 
                "billingTel=" ).append( billingTel ).append( "\n" ).append( 
                "billingEmail=" ).append( billingEmail ).append( "\n\n" ).append( 
                "shipToCP=" ).append( shipToCP ).append( "\n" ).append( 
                "shipToDireccion=" ).append( shipToDireccion ).append( "\n" ).append( 
                "shipToNombre=" ).append( shipToNombre ).append( "\n" ).append( 
                "shipToApellido=" ).append( shipToApellido ).append( "\n" ).append( 
                "shipToTel=" ).append( shipToTel ).append( "\n" ).append( 
                "shipToEmail=" ).append( shipToEmail ).append( "\n\n"  ).append( 
                "serviceIdentifier=" ).append( serviceIdentifier ).append( "\n"  ).append( 
                "typeIdentifier=" ).append( typeIdentifier ).append( "\n").toString();
    }
    
    public void processResponse(String amexResponse)
    {        
        String[] lineas = amexResponse.split("\n");
        
        if(lineas!=null && lineas.length>0)
        {
            for(int i=0; i<lineas.length; i++)
            {
                String[] parms = lineas[i].split("::");
                if(parms!=null && parms.length>0)
                {
                    //APPROVAL CODE
                    if(parms[0].trim().equals("APPROVAL CODE"))
                        approvalCode = parms[1].trim();
                    //ACQUIRER REFERENCE DATA
                    if(parms[0].trim().equals("ACQUIRER REFERENCE DATA"))
                        accReferenceData = parms[1].trim();
                    //ACQUIRER REFERENCE DATA
                    if(parms[0].trim().equals("ACTION CODE"))
                    {
                        setActionCode(parms[1].trim());
                        setActionDsc(parms[2].trim());
                    }
                }
            }
        }
        
    }
    
    /**
     * @return the primaryAccountNumber
     */
    public String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    /**
     * @param primaryAccountNumber the primaryAccountNumber to set
     */
    public void setPrimaryAccountNumber(String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }

    /**
     * @return the cardExpirationDate
     */
    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    /**
     * @param cardExpirationDate the cardExpirationDate to set
     */
    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    /**
     * @return the systemTraceAuditNumber
     */
    public String getSystemTraceAuditNumber() {
        return systemTraceAuditNumber;
    }

    /**
     * @param systemTraceAuditNumber the systemTraceAuditNumber to set
     */
    public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
        this.systemTraceAuditNumber = systemTraceAuditNumber;
    }

    /**
     * @return the localTransactionDateAndTime
     */
    public String getLocalTransactionDateAndTime() {
        return localTransactionDateAndTime;
    }

    /**
     * @param localTransactionDateAndTime the localTransactionDateAndTime to set
     */
    public void setLocalTransactionDateAndTime(String localTransactionDateAndTime) {
        this.localTransactionDateAndTime = localTransactionDateAndTime;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the cid
     */
    public String getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(String cid) {
        this.cid = cid;
    }

    /**
     * @return the messageType
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * @param messageType the messageType to set
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * @return the billingCP
     */
    public String getBillingCP() {
        return billingCP;
    }

    /**
     * @param billingCP the billingCP to set
     */
    public void setBillingCP(String billingCP) {
        this.billingCP = billingCP;
    }

    /**
     * @return the billingDireccion
     */
    public String getBillingDireccion() {
        return billingDireccion;
    }

    /**
     * @param billingDireccion the billingDireccion to set
     */
    public void setBillingDireccion(String billingDireccion) {
        this.billingDireccion = billingDireccion;
    }

    /**
     * @return the billingNombre
     */
    public String getBillingNombre() {
        return billingNombre;
    }

    /**
     * @param billingNombre the billingNombre to set
     */
    public void setBillingNombre(String billingNombre) {
        this.billingNombre = billingNombre;
    }

    /**
     * @return the billingApellido
     */
    public String getBillingApellido() {
        return billingApellido;
    }

    /**
     * @param billingApellido the billingApellido to set
     */
    public void setBillingApellido(String billingApellido) {
        this.billingApellido = billingApellido;
    }

    /**
     * @return the billingTel
     */
    public String getBillingTel() {
        return billingTel;
    }

    /**
     * @param billingTel the billingTel to set
     */
    public void setBillingTel(String billingTel) {
        this.billingTel = billingTel;
    }

    /**
     * @return the billingEmail
     */
    public String getBillingEmail() {
        return billingEmail;
    }

    /**
     * @param billingEmail the billingEmail to set
     */
    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    /**
     * @return the shipToCP
     */
    public String getShipToCP() {
        return shipToCP;
    }

    /**
     * @param shipToCP the shipToCP to set
     */
    public void setShipToCP(String shipToCP) {
        this.shipToCP = shipToCP;
    }

    /**
     * @return the shipToDireccion
     */
    public String getShipToDireccion() {
        return shipToDireccion;
    }

    /**
     * @param shipToDireccion the shipToDireccion to set
     */
    public void setShipToDireccion(String shipToDireccion) {
        this.shipToDireccion = shipToDireccion;
    }

    /**
     * @return the shipToNombre
     */
    public String getShipToNombre() {
        return shipToNombre;
    }

    /**
     * @param shipToNombre the shipToNombre to set
     */
    public void setShipToNombre(String shipToNombre) {
        this.shipToNombre = shipToNombre;
    }

    /**
     * @return the shipToApellido
     */
    public String getShipToApellido() {
        return shipToApellido;
    }

    /**
     * @param shipToApellido the shipToApellido to set
     */
    public void setShipToApellido(String shipToApellido) {
        this.shipToApellido = shipToApellido;
    }

    /**
     * @return the shipToTel
     */
    public String getShipToTel() {
        return shipToTel;
    }

    /**
     * @param shipToTel the shipToTel to set
     */
    public void setShipToTel(String shipToTel) {
        this.shipToTel = shipToTel;
    }

    /**
     * @return the shipToEmail
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * @param shipToEmail the shipToEmail to set
     */
    public void setShipToEmail(String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }

    /**
     * @return the numberAmount
     */
    public double getNumberAmount() {
        return numberAmount;
    }

    /**
     * @param numberAmount the numberAmount to set
     */
    public void setNumberAmount(double numberAmount) {
        this.numberAmount = numberAmount;
    }

    /**
     * @return the serviceIdentifier
     */
    public String getServiceIdentifier() {
        return serviceIdentifier;
    }

    /**
     * @param serviceIdentifier the serviceIdentifier to set
     */
    public void setServiceIdentifier(String serviceIdentifier) {
        this.serviceIdentifier = serviceIdentifier;
    }

    /**
     * @return the typeIdentifier
     */
    public String getTypeIdentifier() {
        return typeIdentifier;
    }

    /**
     * @param typeIdentifier the typeIdentifier to set
     */
    public void setTypeIdentifier(String typeIdentifier) {
        this.typeIdentifier = typeIdentifier;
    }

    /**
     * @return the accReferenceData
     */
    public String getAccReferenceData() {
        return accReferenceData;
    }

    /**
     * @param accReferenceData the accReferenceData to set
     */
    public void setAccReferenceData(String accReferenceData) {
        this.accReferenceData = accReferenceData;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the installations
     */
    public String getInstallations() {
        return installations;
    }

    /**
     * @param installations the installations to set
     */
    public void setInstallations(String installations) {
        this.installations = installations;
    }

    /**
     * @return the dpp
     */
    public String getDpp() {
        return dpp;
    }

    /**
     * @param dpp the dpp to set
     */
    public void setDpp(String dpp) {
        this.dpp = dpp;
    }

    /**
     * @return the processingCode
     */
    public String getProcessingCode() {
        return processingCode;
    }

    /**
     * @param processingCode the processingCode to set
     */
    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    /**
     * @return the approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * @param approvalCode the approvalCode to set
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    /**
     * @return the actionCode
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * @param actionCode the actionCode to set
     */
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    /**
     * @return the actionDsc
     */
    public String getActionDsc() {
        return actionDsc;
    }

    /**
     * @param actionDsc the actionDsc to set
     */
    public void setActionDsc(String actionDsc) {
        this.actionDsc = actionDsc;
    }

    /**
     * @return the acceptorId
     */
    public String getAcceptorId() {
        return acceptorId;
    }

    /**
     * @param acceptorId the acceptorId to set
     */
    public void setAcceptorId(String acceptorId) {
        this.acceptorId = acceptorId;
    }
        
    
            
}
