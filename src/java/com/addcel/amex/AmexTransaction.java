package com.addcel.amex;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.americanexpress.ips.AuthorizationService;
import com.americanexpress.ips.connection.PropertyReader;
import com.americanexpress.ips.enums.FunctionCode;
import com.americanexpress.ips.enums.MessageReasonCode;
import com.americanexpress.ips.gcag.bean.AdditionalDataNationalBean;
import com.americanexpress.ips.gcag.bean.AuthorizationRequestBean;
import com.americanexpress.ips.gcag.bean.AuthorizationResponseBean;
import com.americanexpress.ips.gcag.bean.CardAcceptorNameLocationBean;
import com.americanexpress.ips.gcag.bean.ErrorObject;
import com.americanexpress.ips.gcag.bean.OriginalDataElementsBean;
import com.americanexpress.ips.gcag.bean.PrivateUseData2Bean;
import com.americanexpress.ips.messageformatter.AuthorizationMessageFormatter;


/**
 *
 * @author ADDCEL13
 */
public class AmexTransaction {
	
	private static final Logger logger = LoggerFactory.getLogger(AmexTransaction.class);
    
    //Field 1 Message Type 
    //1100 Authorization    
    private String messageType = "";
    
    //Field 2 Primary Account Number (PAN) (Agregar "15" antes de
    private String primaryAccountNumber = "";
    
    //Field 3 Processing Code
    /*
     * 004000 = Card Authorization Request
     * 004800 = Combination Automated Address Verification (AAV) and Authorization
     * 034000 = AMEX Emergency Check Cashing
     * 064000 = AMEX Travelers Cheque Encashment
     * 174800 = Transaction for Automated Address Verification (AAV) Only
     */
    private String processingCode="";
    
    //Field 4 Amount
    /*
     * For example, for US Dollar (840) transactions, two decimal places are implied. 
     * Thus, the value $100.00 would be entered as:
     * "000000010000"
     */
    private String amount="";
    
    //Field 7 Date and Time
    private String yymmdd="";
    private String hhmmss="";			
    private String yymmddhhmmss="";	
    
    //Field 11 - Unique transaction Id
    private String transactionId = "";
    
    //Field 14 - Expiration Card Date YYMM
    private String cardExpirationDate = ""; 
    
    //Field 19 - Country Code
    private String countryCode = "";
    
    //Field 22 - POS Point of Service Code - 1000S0100130 1000S0100000
    private String pos = "";
    
    //Field 24 - Function Code
    /*
     * 100 - Authorization Request
     * 108 - Authorization Inquiry Request
     * 180 - Batch Authorization
     * 181 - Prepaid Card Partial Authorization Supported
     * 182 - Prepaid Card Authorization with Balance Return Sup-ported
     * 831 - Prepaid Card Authorization with Balance Return Sup-ported
     */
    private String functionCode = "";
    
    /*
     * Field 25 - Message Reason Code
     * 
     * messageReason1->1900
     * messageReason2->1400
     * messageReason3->8700    
     * 
     */
    private String messageReason="";
    
    //Field 26 - Card Acceptor Business Code
    /*
     * 5045 - Computers, Computer Peripheral Equipment, and Software
     * 4829 - Wire Transfers and Money Orders
     * 5046 - Commercial Equipment - Not Elsewhere Classified
     */
    private String cardAcceptorBusiness="";
    
    //Field 27 - Approval Code Lenght : 6 or 2
    private String approvalCodeLenght="";
    
    //Field 31 - Acquirer Reference "Data UNUSED"
    private String acquirerReferenceData = "";
    
    //Field 32 - Acquirer Institution Identification Code : \  - NO ENVIAR
    private String acquirerInstitutionIdentificationCode = "";
            
    //Field 33 - FORWARDING INSTITUTION IDENTIFICATION CODE NOt Present  - NO ENVIAR
    private String forwardInstitutionIdCode = "";
    
    //Field 35 - Track Data 2 Not used, Card Not Present
    
    //Field 37 - Retrieval Reference Number : Transaction number recomended
    private String retrievalReferenceNumber = "";
    
    //Field 41 - Card Acceptor Terminal Identificator: Recomended System Id (MobileCard, MobileWallet, PayBack)
    private String cardAcceptorTerminalId = "";
            
    //Field 42 - Card Acceptor Identification Code: Merchant ID. 
    private String cardAcceptorId = "";
    
    //Field 43 - Card Acceptor Name/Location
    private String tppName = "";
    private String tppStreet = "";
    private String tppCity = "";
    private String tppPostalCode = "";
    
    //Field 45 - Track 1 Data: No Aplica
    
    //Field 47 - Additional Data - National
    private String itdSecondaryKey="";
    private String itdCustomerEmail="";
    private String itdCustomerHostName="";
    private String itdHttpBrowserType="";
    private String itdShipToCountry="";
    private String itdShippingMethod="";
    private String itdMerchantProductSku="";
    private String itdCustomerIP="";
    private String itdCustomerANI="";
    private String itdCustomerInfoIdentifier="";
    
    //Field 48 - Additional Data - Private : Not Needed
    
    //Field 49 - Currency Code
    private String currencyCode="";
    
    //Field 52 Personal Identification Data: Not Used
    
    //Field 53 Security Related Control Information: CID
    private String cardIdentifierCode="";
    
    //Field 55 Integrated Circuit Card System Related Data
    
    //Field 60 National Use Data - Not Used
    
    //Field 61 National Use Data - Not Used
    
    //Field 62 Private use data - Not Used
    
    //Field 63 Private use data - Optional for AAV
    //** ATENCION ESTE SE USA CON AAV 004800 Y 174800
    private String cmServiceIdentifier = "";
    private String cmRequestTypeIdentifier = "";
    private String cmBillingName = "";
    private String cmBillingFirstName = "";
    private String cmBillingLastName = "";
    private String cmBillingPostalCode = "";
    private String cmBillingAddress = "";
    private String cmBillingPhoneNumber = "";
    private String cmShipToPostalCode = "";
    private String cmShipToAddress = "";
    private String cmShipToName = "";
    private String cmShipToFirstName = "";
    private String cmShipToLastName = "";
    private String cmShipToPhone = "";
    private String cmShipToCountryCode = "";
    
    private String NEW_LINE_CHAR = "\n";
    
    public AmexTransaction()
    {   
        yymmdd=getLocalDateTime("yyMMdd");
        hhmmss=getLocalDateTime("hhmmss");			
        yymmddhhmmss=yymmdd+hhmmss;	
        
        
        //Todo esto se toma de la base de datos k
        //Third Party Processor data
        //tppName = "RADIOCOMUNICACIONES Y SOLUCIONES CELULARES";
        tppName = AmexConstants.AMEX_TPP_NAME;//"S#ADDCEL.COM";
        tppStreet = AmexConstants.AMEX_TPP_STREET;//"PASEO DE LA ASUNCION";
        tppCity = AmexConstants.AMEX_TPP_CITY;//"MEXICO";
        tppPostalCode = AmexConstants.AMEX_TPP_POSTALCODE;//"52149";
        
        approvalCodeLenght=AmexConstants.AMEX_APPROVAL_CODE_LENGHT;//"6";
        //retrievalReferenceNumber = this.transactionId;
        cardAcceptorTerminalId = AmexConstants.AMEX_TERMINAL;//"1"; //MobileCard
        
        
        //Este es la afiliaci�n, debe de venir por par�metro
        //cardAcceptorId = AmexConstants.AMEX_ACCEPTOR_ID;//"9351358974";    //Asignado por AMEX
        
        
        countryCode = AmexConstants.AMEX_COUNTRY_CODE;//CountryCode.Mexico.getCountryCode();
        pos=AmexConstants.AMEX_POS;//"1000S0S00000"; //"1000S0100000"
        
        currencyCode = AmexConstants.AMEX_CURRENCY_CODE;//CurrencyCode.Mexican_Peso.getCurrencyCode();
        acquirerReferenceData = AmexConstants.AMEX_ACQUIRER_REFERENCE_DATA;//"000071169033272";
        
        forwardInstitutionIdCode = AmexConstants.AMEX_FORWARD_INSTITUTION_ID_CODE;//"\\";
    }
    
    /*
     * TRANSACCIONES PROBADAS CON AMEX
     */
    
    //1804 Network Management Request
    public AmexResponse networkManagementRequest() throws AmexException
    {
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        //Datos específicos para 1804
        messageType = "1804";
        processingCode = "000000";
        functionCode = FunctionCode.SYSTEM_AUDIT_CONTROL_ECHO_TEST.getFunctionCode();        
        messageReason = MessageReasonCode.value3.getMessageReasonCode();
        
        int transaction = TransactionControl.createTransaction(
                yymmddhhmmss,"test", "Amex Test NwRq", "sburgara", "", "", "", "", "", "", "", "", 
                "", "", "666", "", "","");
        transactionId = setTransactionId((long)transaction);        

        
        authRequestBean.setMessageTypeIdentifier(messageType);
        authRequestBean.setProcessingCode(processingCode);
        logger.debug(functionCode);
        authRequestBean.setFunctionCode(functionCode);
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        authRequestBean.setMessageReasonCode(messageReason);
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss);

        
        
        AmexResponse response = sendRequest(authRequestBean, authResponseBean);
        
        TransactionControl.updateTransaction(transaction, response);
        
        return response;
        //return sendRequestSimple(authRequestBean, authResponseBean);
    }

    public AmexResponse cardAuthorizationRequestAAV(AmexData data) throws AmexException
    {
//        //004800
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        //Datos específicos para 1100 Authorization Request Message
        //004000
        
        if(!verifyCardNumber(data.getPrimaryAccountNumber()))
        {
            AmexResponse res = new AmexResponse();
            res.setError("-666", "Tarjeta no valida");
            return res;
            //throw new AmexException("1001","Invalid Card Number");
        }
        
        
        //Data Test from AMEX Test Script MX-BA-484-?-001
        messageType = AmexConstants.AMEX_AUTHORIZATION_AAV_MESSAGE_TYPE;
        processingCode = data.getProcessingCode(); //AmexConstants.AMEX_AUTHORIZATION_AAV_PROCESSING_CODE;  //Card Authorization Request
        primaryAccountNumber = data.getPrimaryAccountNumber();//"376677849952008";
        cardExpirationDate = data.getCardExpirationDate();//"1211";        
        functionCode = AmexConstants.AMEX_AUTHORIZATION_AAV_FUNCTION_CODE;//FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
        amount=setAmount(data.getNumberAmount());
        cardAcceptorBusiness = AmexConstants.AMEX_AUTHORIZATION_AAV_ACCEPTOR_BUSINESS;//MCCCode.Miscellaneous_General_Merchandise.getMccCode();
        cardIdentifierCode = data.getCid();//"2234";
        cardAcceptorId = data.getAcceptorId();
        
        
        
        itdSecondaryKey=AmexConstants.AMEX_AUTHORIZATION_AAV_ITD_SECONDARY_KEY;
        itdCustomerEmail=data.getBillingEmail();
        itdCustomerHostName=AmexConstants.AMEX_AUTHORIZATION_AAV_ITD_CUSTOMER_HOST;
        itdHttpBrowserType="";
        itdShipToCountry=AmexConstants.AMEX_AUTHORIZATION_AAV_SHIPPING_TO_COUNTRY;            //ISO 3166
        itdShippingMethod=AmexConstants.AMEX_AUTHORIZATION_AAV_SHIPPING_METHOD; //"02";             //??
        itdMerchantProductSku="";
        itdCustomerIP=AmexConstants.AMEX_AUTHORIZATION_AAV_ITD_CUSTOMER_IP;
        itdCustomerANI="";
        itdCustomerInfoIdentifier=AmexConstants.AMEX_AUTHORIZATION_AAV_INFO_IDENTIFIER;;        
        
        messageReason = AmexConstants.AMEX_AUTHORIZATION_AAV_MESSAGE_IDENTIFIER;//MessageReasonCode.value1.getMessageReasonCode();
        approvalCodeLenght=AmexConstants.AMEX_AUTHORIZATION_AAV_APPROVAL_CODE_LENGHT;

        //AAV
        cmServiceIdentifier = data.getServiceIdentifier();//AmexConstants.AMEX_AUTHORIZATION_AAV_CM_SERVICE_IDENTIFIER;
        cmRequestTypeIdentifier = data.getTypeIdentifier();//AmexConstants.AMEX_AUTHORIZATION_AAV_CM_REQUEST_TYPE_IDENTIFIER_3378;//"AD";
        cmBillingPostalCode = data.getBillingCP();
        cmBillingAddress = data.getBillingDireccion();
        cmBillingName = data.getBillingNombre() + " " + data.getBillingApellido();
        cmBillingFirstName = data.getBillingNombre();
        cmBillingLastName = data.getBillingApellido();;
        cmBillingPhoneNumber = data.getBillingTel();
        cmShipToPostalCode = data.getShipToCP();
        cmShipToAddress = data.getShipToDireccion();
        cmShipToName = data.getShipToNombre() + " " + data.getShipToApellido();
        cmShipToPhone = data.getShipToTel();
        cmShipToCountryCode = AmexConstants.AMEX_AUTHORIZATION_AAV_CM_SHIP_TO_COUNTRY_CODE;
        
        
        acquirerInstitutionIdentificationCode = AmexConstants.AMEX_AUTHORIZATION_AAV_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE;
        //acquirerInstitutionIdentificationCode = "\\";
        //Information Verification - No Disponible
        
        int transaction = TransactionControl.createTransaction(
                yymmddhhmmss,messageReason, "Authorization AAV", "admin", primaryAccountNumber, cardExpirationDate, amount, cardIdentifierCode, 
                cmBillingFirstName, cmBillingLastName, cmBillingAddress, cmBillingPhoneNumber, "", cmShipToPostalCode, "666", 
                messageType, processingCode,cardAcceptorId);
        transactionId = setTransactionId((long)transaction);                               


        data.setSystemTraceAuditNumber(transactionId);
        data.setAmount(setAmount(data.getNumberAmount()));
        data.setMessageType(messageType);
        data.setLocalTransactionDateAndTime(yymmddhhmmss);
                
        authRequestBean.setMessageTypeIdentifier(messageType);
        authRequestBean.setProcessingCode(processingCode);
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        //authRequestBean.setFunctionCode("100");//setFunctionCode(functionCode);
        if(data.getPaymentType()!=null && data.getPaymentType().equals("05"))
            authRequestBean.setFunctionCode(data.getDpp());
        else
            authRequestBean.setFunctionCode(functionCode);
        
        logger.debug("004"+data.getPaymentType()+data.getInstallations());
        if(data.getPaymentType()!=null && !data.getPaymentType().equals(""))
        {
            //authRequestBean.setAdditionalDataPrivate("004" + data.getPaymentType() + data.getInstallations());
            authRequestBean.setAdditionalDataPrivate(data.getPaymentType() + data.getInstallations());
        }
                
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        authRequestBean.setAmountTransaction(amount);	
        authRequestBean.setPosDataCode(pos);
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        authRequestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId); //ESTABA COMENTADO
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId.trim());
        authRequestBean.setTransactionCurrencyCode(currencyCode);	

        //Card Acceptor Name/Location #
        CardAcceptorNameLocationBean cardAcceptNameLocationBean = new CardAcceptorNameLocationBean();
        
        cardAcceptNameLocationBean.setSubfield1Name(tppName);
        cardAcceptNameLocationBean.setSubfield1Street(tppStreet);
        cardAcceptNameLocationBean.setSubfield1City(tppCity);
        cardAcceptNameLocationBean.setSubfield2PostalCode(tppPostalCode);
                
        authRequestBean.setCardAcceptorNameLocation(cardAcceptNameLocationBean.populateCardAccpetorNameLocation(authResponseBean));
        
        //Additional Data National
        AdditionalDataNationalBean additionalDataNationalBean = new AdditionalDataNationalBean();

        additionalDataNationalBean.setSecondaryId(itdSecondaryKey);
        additionalDataNationalBean.setCustomerEmail(itdCustomerEmail);
        additionalDataNationalBean.setCustomerHostname(itdCustomerHostName);
        additionalDataNationalBean.setHttpBrowserType(itdHttpBrowserType);
        additionalDataNationalBean.setShipToCountry(itdShipToCountry);
        additionalDataNationalBean.setShippingMethod(itdShippingMethod);
        additionalDataNationalBean.setMerchantProductSku(itdMerchantProductSku);
        additionalDataNationalBean.setCustomerIP(itdCustomerIP);
        additionalDataNationalBean.setCustomerANI(itdCustomerANI);
        
        //CUSTOMER II DIGITS
        //additionalDataNationalBean.setCustomerInfoIdentifier(itdCustomerInfoIdentifier);
        authRequestBean.setAdditionalDataNational(additionalDataNationalBean.populateAdditionalDataNational(authResponseBean));
        
        
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        
        authRequestBean.setMessageReasonCode(messageReason);
        //TEST
        //authRequestBean.setApprovalCodeLength(approvalCodeLenght);                        
        authRequestBean.setCardIdentifierCode(cardIdentifierCode);
        
        authRequestBean.setAcquiringInstitutionIdCode(acquirerInstitutionIdentificationCode);
        
        
        PrivateUseData2Bean privateUseData2Bean = new PrivateUseData2Bean();
           //** For 33 byte format - Start
        logger.debug("Using 33 byte format");
        privateUseData2Bean.setServiceIdentifier(cmServiceIdentifier);
        logger.debug("Service Identifier: "+privateUseData2Bean.getServiceIdentifier());
        privateUseData2Bean.setRequestTypeIdentifier(cmRequestTypeIdentifier);
        logger.debug("Request Type Identifier: "+privateUseData2Bean.getRequestTypeIdentifier());
        privateUseData2Bean.setCmBillingPostalcode(cmBillingPostalCode);
        logger.debug("Billing Postal: "+privateUseData2Bean.getCmBillingPostalcode());
        privateUseData2Bean.setCmBillingAddress(cmBillingAddress);
        logger.debug("Billing Address: "+privateUseData2Bean.getCmBillingAddress());
        logger.debug("Function Code: " + authRequestBean.getFunctionCode());
        
        if(
           (cmBillingFirstName!=null && !cmBillingFirstName.equals("")) ||
           (cmBillingFirstName!=null && !cmBillingFirstName.equals(""))
           )
        {
			logger.debug("Using 78 byte format");
			privateUseData2Bean.setCmFirstName(cmBillingFirstName);
			privateUseData2Bean.setCmLastName(cmBillingLastName);
			if(
			  (cmBillingPhoneNumber!=null && !cmBillingPhoneNumber.equals("")) ||
			  (cmShipToPostalCode!=null && !cmShipToPostalCode.equals("")) ||
			  (cmShipToAddress!=null && !cmShipToAddress.equals("")) ||
			  (cmShipToFirstName!=null && !cmShipToFirstName.equals("")) ||
			  (cmShipToLastName!=null && !cmShipToLastName.equals(""))
			          )
			{
				logger.debug("Using 205 byte format");
				privateUseData2Bean.setRequestTypeIdentifier(AmexConstants.AMEX_AUTHORIZATION_AAV_CM_REQUEST_TYPE_IDENTIFIER);
			  //privateUseData2Bean.setRequestTypeIdentifier("AD");
				privateUseData2Bean.setCmBillingPhoneNumber(cmBillingPhoneNumber);
				privateUseData2Bean.setShipToPostalCode(cmShipToPostalCode);
				privateUseData2Bean.setShipToAddress(cmShipToAddress);
				privateUseData2Bean.setShipToFirstname(cmShipToFirstName);
				privateUseData2Bean.setShipToLastName(cmShipToLastName);
				privateUseData2Bean.setShipToPhoneNumber(cmBillingPhoneNumber);
				privateUseData2Bean.setShipToCountryCode(cmShipToCountryCode);
			          
			}
		}
        //privateUseData2Bean
        //** For 33 byte format - End
        
        //logger.debug(data);                                        
        authRequestBean.setVerificationInformation(privateUseData2Bean.populatePrivateUseData(authResponseBean));        
                                
        //return sendRequestSimple(authRequestBean, authResponseBean);                
        AmexResponse response = sendRequest(authRequestBean, authResponseBean);
        TransactionControl.updateTransaction(transaction, response, response.getAccReference());
        
        return response;
        
    }
    
    /*
    public AmexResponse cardAuthorizationRequestAAV(String card, String expiration, double fee, String cid,
            String email, String cp, String address, String firstName, String lastName, String phone  ) throws AmexException
    {
//        //004800
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        //Datos específicos para 1100 Authorization Request Message
        //004000
        
        if(!verifyCardNumber(card))
            throw new AmexException("1001","Invalid Card Number");
        
        
        //Data Test from AMEX Test Script MX-BA-484-?-001
        messageType = AmexConstants.AMEX_AUTHORIZATION_AAV_MESSAGE_TYPE;
        processingCode = AmexConstants.AMEX_AUTHORIZATION_AAV_PROCESSING_CODE;  //Card Authorization Request
        primaryAccountNumber = card;//"376677849952008";
        cardExpirationDate = expiration;//"1211";        
        functionCode = AmexConstants.AMEX_AUTHORIZATION_AAV_FUNCTION_CODE;//FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
        amount=setAmount(fee);
        cardAcceptorBusiness = AmexConstants.AMEX_AUTHORIZATION_AAV_ACCEPTOR_BUSINESS;//MCCCode.Miscellaneous_General_Merchandise.getMccCode();
        cardIdentifierCode = cid;//"2234";
        
        
        
        itdSecondaryKey=AmexConstants.AMEX_AUTHORIZATION_AAV_ITD_SECONDARY_KEY;
        itdCustomerEmail=email;
        itdCustomerHostName=AmexConstants.AMEX_AUTHORIZATION_AAV_ITD_CUSTOMER_HOST;
        itdHttpBrowserType="";
        itdShipToCountry=AmexConstants.AMEX_AUTHORIZATION_AAV_SHIPPING_TO_COUNTRY;            //ISO 3166
        itdShippingMethod=AmexConstants.AMEX_AUTHORIZATION_AAV_SHIPPING_METHOD; //"02";             //??
        itdMerchantProductSku="";
        itdCustomerIP=AmexConstants.AMEX_AUTHORIZATION_AAV_ITD_CUSTOMER_IP;
        itdCustomerANI="";
        itdCustomerInfoIdentifier=AmexConstants.AMEX_AUTHORIZATION_AAV_INFO_IDENTIFIER;;        
        
        messageReason = AmexConstants.AMEX_AUTHORIZATION_AAV_MESSAGE_IDENTIFIER;//MessageReasonCode.value1.getMessageReasonCode();
        approvalCodeLenght=AmexConstants.AMEX_AUTHORIZATION_AAV_APPROVAL_CODE_LENGHT;

        //AAV
        cmServiceIdentifier = AmexConstants.AMEX_AUTHORIZATION_AAV_CM_SERVICE_IDENTIFIER;
        cmRequestTypeIdentifier = AmexConstants.AMEX_AUTHORIZATION_AAV_CM_REQUEST_TYPE_IDENTIFIER;//"AD";
        cmBillingPostalCode = cp;
        cmBillingAddress = address;
        cmBillingName = firstName + " " + lastName;
        cmBillingFirstName = firstName;
        cmBillingLastName = lastName;
        cmBillingPhoneNumber = phone;
        cmShipToPostalCode = cp;
        cmShipToAddress = address;
        cmShipToName = firstName + " " + lastName;
        cmShipToPhone = phone;
        cmShipToCountryCode = AmexConstants.AMEX_AUTHORIZATION_AAV_CM_SHIP_TO_COUNTRY_CODE;
        
        
        acquirerInstitutionIdentificationCode = AmexConstants.AMEX_AUTHORIZATION_AAV_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE;
        //acquirerInstitutionIdentificationCode = "\\";
        //Information Verification - No Disponible
        
        int transaction = TransactionControl.createTransaction(
                yymmddhhmmss,messageReason, "Authorization AAV", "admin", primaryAccountNumber, cardExpirationDate, amount, cardIdentifierCode, 
                cmBillingFirstName, cmBillingLastName, cmBillingAddress, cmBillingPhoneNumber, "", cmShipToPostalCode, "666", 
                messageType, processingCode);
        transactionId = setTransactionId((long)transaction);                               
            
        authRequestBean.setMessageTypeIdentifier(messageType);
        authRequestBean.setProcessingCode(processingCode);
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        authRequestBean.setFunctionCode(functionCode);
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        authRequestBean.setAmountTransaction(amount);	
        authRequestBean.setPosDataCode(pos);
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        authRequestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId); //ESTABA COMENTADO
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId);
        authRequestBean.setTransactionCurrencyCode(currencyCode);	

        //Card Acceptor Name/Location #
        CardAcceptorNameLocationBean cardAcceptNameLocationBean = new CardAcceptorNameLocationBean();
        
        cardAcceptNameLocationBean.setSubfield1Name(tppName);
        cardAcceptNameLocationBean.setSubfield1Street(tppStreet);
        cardAcceptNameLocationBean.setSubfield1City(tppCity);
        cardAcceptNameLocationBean.setSubfield2PostalCode(tppPostalCode);
                
        authRequestBean.setCardAcceptorNameLocation(cardAcceptNameLocationBean.populateCardAccpetorNameLocation(authResponseBean));
        
        //Additional Data National
        AdditionalDataNationalBean additionalDataNationalBean = new AdditionalDataNationalBean();

        additionalDataNationalBean.setSecondaryId(itdSecondaryKey);
        additionalDataNationalBean.setCustomerEmail(itdCustomerEmail);
        additionalDataNationalBean.setCustomerHostname(itdCustomerHostName);
        additionalDataNationalBean.setHttpBrowserType(itdHttpBrowserType);
        additionalDataNationalBean.setShipToCountry(itdShipToCountry);
        additionalDataNationalBean.setShippingMethod(itdShippingMethod);
        additionalDataNationalBean.setMerchantProductSku(itdMerchantProductSku);
        additionalDataNationalBean.setCustomerIP(itdCustomerIP);
        additionalDataNationalBean.setCustomerANI(itdCustomerANI);
        
        //CUSTOMER II DIGITS
        //additionalDataNationalBean.setCustomerInfoIdentifier(itdCustomerInfoIdentifier);
        authRequestBean.setAdditionalDataNational(additionalDataNationalBean.populateAdditionalDataNational(authResponseBean));
        
        
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        
        authRequestBean.setMessageReasonCode(messageReason);
        authRequestBean.setApprovalCodeLength(approvalCodeLenght);                        
        authRequestBean.setCardIdentifierCode(cardIdentifierCode);
        
        authRequestBean.setAcquiringInstitutionIdCode(acquirerInstitutionIdentificationCode);
        
	 PrivateUseData2Bean privateUseData2Bean = new PrivateUseData2Bean();
           //** For 33 byte format - Start
          privateUseData2Bean.setServiceIdentifier(cmServiceIdentifier);
          privateUseData2Bean.setRequestTypeIdentifier(cmRequestTypeIdentifier);
          privateUseData2Bean.setCmBillingPostalcode(cmBillingPostalCode);
          privateUseData2Bean.setCmBillingAddress(cmBillingAddress);
          privateUseData2Bean.setCmFirstName(cmBillingFirstName);
          privateUseData2Bean.setCmLastName(cmBillingLastName);
          privateUseData2Bean.setCmBillingPhoneNumber(cmBillingPhoneNumber);
          privateUseData2Bean.setShipToPostalCode(cmShipToPostalCode);
          privateUseData2Bean.setShipToAddress(cmShipToAddress);
          privateUseData2Bean.setShipToFirstname(cmShipToFirstName);
          privateUseData2Bean.setShipToLastName(cmShipToLastName);
          privateUseData2Bean.setShipToPhoneNumber(cmBillingPhoneNumber);
          privateUseData2Bean.setShipToCountryCode(cmShipToCountryCode);
          //privateUseData2Bean
          //** For 33 byte format - End
                    
          String _res = 
          "\n*************\n" +
          "getMessageTypeIdentifier()->"+authRequestBean.getMessageTypeIdentifier() + "\n" +
          "getSystemTraceAuditNumber()->"+authRequestBean.getSystemTraceAuditNumber() + "\n" +
          "getLocalTransactionDateAndTime()->"+authRequestBean.getLocalTransactionDateAndTime() + "\n" +
          "getAcquiringInstitutionIdCode()->"+authRequestBean.getAcquiringInstitutionIdCode() +  "\n";
//          "getCmBillingPostalcode()->"+privateUseData2Bean.getCmBillingPostalcode() +  "\n" +
//          "getCmBillingAddress()->"+privateUseData2Bean.getCmBillingAddress() +  "\n" +
//          "getCmFirstName()->"+privateUseData2Bean.getCmFirstName() +  "\n" +
//          "getCmLastName()->"+privateUseData2Bean.getCmLastName() +  "\n" +
//          "getCmBillingPhoneNumber()->"+privateUseData2Bean.getCmBillingPhoneNumber() +  "\n" +
//          "getShipToPostalCode()->"+privateUseData2Bean.getShipToPostalCode() +  "\n" +
//          "getShipToAddress()->"+privateUseData2Bean.getShipToAddress() +  "\n" +
//          "getShipToFirstName()->"+privateUseData2Bean.getShipToFirstName() +  "\n" +
//          "getShipToLastName()->"+privateUseData2Bean.getShipToLastName() +  "\n" +
//          "getShipToPhoneNumber()->"+privateUseData2Bean.getShipToPhoneNumber() +  "\n" +
//          "getShipToCountryCode()->"+privateUseData2Bean.getShipToCountryCode();
          
//          logger.debug(_res);
          
          authRequestBean.setVerificationInformation(privateUseData2Bean.populatePrivateUseData(authResponseBean));        
          
                                
        //return sendRequestSimple(authRequestBean, authResponseBean);                
        AmexResponse response = sendRequest(authRequestBean, authResponseBean);
        
        TransactionControl.updateTransaction(transaction, response);
        
        return response;
    }
    */
    
    public AmexResponse cardReversalAdviceRequest(long reversalTransactionIdn, String reversalTipo)  throws AmexException
    {
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        String originalSystemTraceAuditNumber = setTransactionId(reversalTransactionIdn);
        
        AmexData data = TransactionControl.getData(reversalTransactionIdn,originalSystemTraceAuditNumber);
        logger.debug("**AMEX:\n"+data);
                
        //Datos específicos para 1420 Authorization Request Message        
        messageType = AmexConstants.AMEX_REVERSAL_MESSAGE_TYPE;//"1420";
        primaryAccountNumber = data.getPrimaryAccountNumber();//"376677849952008";
        processingCode = reversalTipo;//AmexConstants.AMEX_REVERSAL_PROCESSING_CODE;//"024000";  //Card Authorization Request
        amount=data.getAmount();
        
        //trace
        //datetime
        cardExpirationDate = data.getCardExpirationDate();        
        //country code
        
        functionCode =AmexConstants.AMEX_REVERSAL_FUNCTION_CODE;
        cardAcceptorBusiness = AmexConstants.AMEX_REVERSAL_CARD_ACCEPTOR_BUSINESS;
        cardIdentifierCode = data.getCid();
        
        
        
        itdSecondaryKey=AmexConstants.AMEX_REVERSAL_SECONDARY_KEY;
        itdCustomerEmail="";
        itdCustomerHostName=AmexConstants.AMEX_REVERSAL_CUSTOMER_HOST;
        itdHttpBrowserType="";
        itdShipToCountry=AmexConstants.AMEX_COUNTRY_CODE;            //ISO 3166
        itdShippingMethod=AmexConstants.AMEX_AUTHORIZATION_AAV_SHIPPING_METHOD;             //??
        itdMerchantProductSku="";
        itdCustomerIP=AmexConstants.AMEX_REVERSAL_CUSTOMER_IP;
        itdCustomerANI="";
        itdCustomerInfoIdentifier=AmexConstants.AMEX_REVERSAL_CUSTOMER_INFO_ID;        
        
        messageReason = AmexConstants.AMEX_REVERSAL_MESSAGE_REASON;//MessageReasonCode.value3.getMessageReasonCode();
        approvalCodeLenght=AmexConstants.AMEX_REVERSAL_APPROVAL_LENGHT_CODE;
                
        int transaction = TransactionControl.createTransaction(
                yymmddhhmmss,messageReason, "Reversal AAV", "admin", primaryAccountNumber, cardExpirationDate, amount, cardIdentifierCode, 
                "", "", "", "", "", "", "666", 
                messageType, processingCode,"");
        transactionId = setTransactionId((long)transaction);                               
        
        //Information Verification - No Disponible                        
        
        //Field 1
        authRequestBean.setMessageTypeIdentifier(messageType);
        //Field 2
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        //Field 3
        authRequestBean.setProcessingCode(processingCode);
        //Field 4
        authRequestBean.setAmountTransaction(amount);	
        //Field 11
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        //Field 12
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        //Field 14
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        //Field 19
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        //Field 22
        authRequestBean.setPosDataCode(pos);
        //Field 25
        authRequestBean.setMessageReasonCode(messageReason);
        //Field 26
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        //Field 31 NOT USED
        //Field 31 - en la respuesta del 1110
        //authRequestBean.setA
        
        
        logger.debug("Setting transactionId: "+data.getAccReferenceData());
        if(reversalTipo.equals("024000"))
        {
            authRequestBean.setTransactionId(data.getAccReferenceData());
        }
        //authRequestBean.setTransactionId(transactionId);
        //authRequestBean.setTransactionId(acquirerReferenceData);
                
        //authRequestBean.setTransactionId(data.getAccReferenceData());
        
        //Field 42
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId);
        //Field 49
        authRequestBean.setTransactionCurrencyCode(currencyCode);	
        
                
        //Field 56
        //Original Data
        OriginalDataElementsBean orignalDataElementsBean = new OriginalDataElementsBean();
        //Field 1
        orignalDataElementsBean.setMessageTypeIdOriginal(data.getMessageType());
        //Field 2
        orignalDataElementsBean.setSystemTraceAuditNumberOriginal(data.getSystemTraceAuditNumber());
        //Field 3
        orignalDataElementsBean.setLocalTransactionDateAndTimeOriginal(data.getLocalTransactionDateAndTime());
        //Field 4
        //orignalDataElementsBean.setAcquiringInstIdOriginal(AmexConstants.AMEX_REVERSAL_ACQUIRING_INST_ID_ORIGINAL); //cardAcceptorId        
        orignalDataElementsBean.setAcquiringInstIdOriginal(""); //cardAcceptorId
        authRequestBean.setOriginalDataElementsBean(orignalDataElementsBean);
        
                                               
        //return sendRequestSimple(authRequestBean, authResponseBean);        
        AmexResponse response = sendRequest(authRequestBean, authResponseBean);
        
        
        logger.debug("AMEX: reversalTransactionIdn: "+reversalTransactionIdn+",  transactionId:"+transactionId);
        TransactionControl.updateReversal(reversalTransactionIdn, Long.parseLong(transactionId));
        
        return response;
        
        
    }
        
    
    /*
     * SIMPLE REQUEST
     */
    /*
    private String sendRequestSimple(AuthorizationRequestBean authRequestBean,AuthorizationResponseBean authResponseBean) throws AmexException
    {
        String res = new String();
        
        AuthorizationService authorizationService  = new AuthorizationService();        
        res+="1.AMEX:Init...\n";        
        
        try 
        {
            if(authResponseBean.getAuthErrorList()== null ||authResponseBean.getAuthErrorList().isEmpty() )
            {
                res+="2.AMEX:getAuthErrorList is null or empty\n";        
            }
            else
            {
                Iterator<ErrorObject> errorList1 =authResponseBean.getAuthErrorList().iterator();
                while (errorList1.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList1.next();
                    res+="3.AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription()+"\n";        
                }
            }
                
            
            //authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);


            if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
            {
                res+="4.AMEX:getAuthErrorList is null or empty\n";       

                try 
                { 
                    PropertyReader propertyReader=new PropertyReader();

                    propertyReader=propertyReader.setProxyDetailsFile("/authorization.properties");
                    //logger.debug("After Setting Property");

                  //Changes made on JUN06_2AM_MST . Calling validateAuthorizationRequest method to set the bean values in the request
                    authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);
                  //Changes made on JUN06_2AM_MST . For debugging the request parameters
                  //  logger.debug("AMEX:**Request:< "+authRequestBean.toString()+" >");

                  //Changes made on JUN06_2AM_MST . Call create and Send method only when there is no validation errors
                    if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
                    {
                        //logger.debug("Inside the if");
                        authResponseBean= authorizationService.createAndSendAuthorizationRequest(authRequestBean,propertyReader);
                        res+="5.AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >\n";
                        res+="6.AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >\n";
                        
                        logger.debug("5.AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >");
                        logger.debug("6.AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >");
                        // ANSWER
                    }

                     res+="7.******Request:\n"+authRequestBean;
                     res+="\n\n8.******Response:\n"+authResponseBean;
                     if ( authResponseBean != null && authResponseBean.getAuthErrorList() != null)
                     {
                        res+="9.AMEX:******Response Errors:\n";
                        Iterator errorlist=authResponseBean.getAuthErrorList().iterator();
                        while(errorlist.hasNext())
                        {
                            ErrorObject errorObject = (ErrorObject) errorlist.next();
                            res+="10.**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription() +"\n";
                        }

                     }
                     else
                         res+="\n\n\n11.******AMEX ANSWER:\nRequest:<"+authResponseBean.getISOFormattedRequest()+">\n\nResponse:<"+authResponseBean.getISOFormattedResponse()+">";


                    authorizationService.flushRequestMemory(authRequestBean);

                    authorizationService.flushResponseMemory(authResponseBean);

                } 
                catch (Exception e) 
                {
                    res+="12.AMEX:**ERROR Authorization: ->"+e.getMessage()+"\n\n*****************************\n";        
                    e.printStackTrace(System.out);
                }
            } 
 
            else
            {
                res+="13.AMEX:**ERRORES getAuthErrorList is null";        
                Iterator<ErrorObject> errorList =authResponseBean.getAuthErrorList().iterator();
                while (errorList.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList.next();
                    res+="14.AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription();        
                }
            }
        } 
        catch (Exception e) 
        {
            res+="15.AMEX:ERROR: "+e.getMessage();
            logger.error("Error: {}" + e.getMessage());
        }        
        return res;
    }
    
     * */
    
    /*
     * send request
     */
    private AmexResponse sendRequest(AuthorizationRequestBean authRequestBean,AuthorizationResponseBean authResponseBean) throws AmexException
    {
        AmexResponse response = new AmexResponse();
        response.transactionId = transactionId;
        AuthorizationService authorizationService  = new AuthorizationService();                
        try 
        {
//            if(authResponseBean.getAuthErrorList()== null ||authResponseBean.getAuthErrorList().isEmpty() )
//            {
//
//            }
            if(authResponseBean.getAuthErrorList()!= null && !authResponseBean.getAuthErrorList().isEmpty())
            {
                Iterator<ErrorObject> errorList1 =authResponseBean.getAuthErrorList().iterator();
                while (errorList1.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList1.next();
                    response.setError(errorObject.getErrorCode(), errorObject.getErrorDescription());
                    //res+="3.AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription()+"\n";        
                }
            }
            
            //authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);
            if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
            {
                try 
                { 
                    PropertyReader propertyReader=new PropertyReader();

                    propertyReader=propertyReader.setProxyDetailsFile("/authorization.properties");
                    //logger.debug("After Setting Property");

                    //Changes made on JUN06_2AM_MST . Calling validateAuthorizationRequest method to set the bean values in the request
                    authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);
                    //Changes made on JUN06_2AM_MST . For debugging the request parameters
                    //  logger.debug("AMEX:**Request:< "+authRequestBean.toString()+" >");

                    //Changes made on JUN06_2AM_MST . Call create and Send method only when there is no validation errors
                    AuthorizationMessageFormatter formatter = new AuthorizationMessageFormatter();
                    logger.debug("\nISO\n"+formatter.createISORequest(authRequestBean)+"\n");                                        
                    logger.debug(authRequestBean+"\n");
                    if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
                    {
                        //logger.debug("Inside the if");
                        authResponseBean= authorizationService.createAndSendAuthorizationRequest(authRequestBean,propertyReader);
//                        response.codigo = authResponseBean.getActionCode();
//                        response.mensaje = authResponseBean.getActionCodeDescription();
//                        response.accReference = authResponseBean.getTransactionId();//authResponseBean.getIccRelatedData();
//                        response.approvalCode = authRequestBean.getApprovalCodeLength();
                        logger.debug("authResponseBean: \n" + authResponseBean);
                        AmexData localAmexData = new AmexData();
                        localAmexData.processResponse(authResponseBean.toString());
                        response.codigo = localAmexData.getActionCode();
                        response.mensaje = localAmexData.getActionDsc();
                        response.setAccReference(localAmexData.getAccReferenceData());
                        response.setApprovalCode(localAmexData.getApprovalCode());
                        
                        logger.debug("******codigo: "+response.codigo);
                        logger.debug("******mensaje: "+response.mensaje);
                        logger.debug("******accReference: "+response.getAccReference());
                        logger.debug("******approvalCode: "+response.getApprovalCode());
                                                
                        logger.debug("5.AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >");
                        logger.debug("6.AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >");                        
                        /** ANSWER **/
                    }

                     if ( authResponseBean != null && authResponseBean.getAuthErrorList() != null)
                     {
                        Iterator errorlist=authResponseBean.getAuthErrorList().iterator();
                        while(errorlist.hasNext())
                        {
                            ErrorObject errorObject = (ErrorObject) errorlist.next();
                            response.setError(errorObject.getErrorCode(), errorObject.getErrorDescription());
                        }

                     }
                    authorizationService.flushRequestMemory(authRequestBean);
                    authorizationService.flushResponseMemory(authResponseBean);

                } 
                catch (Exception e) 
                {
                    response.codigo="0666";
                    response.mensaje= "**Check Log for more details:" + e.getMessage();
                    logger.error("Error: {}", e);
                }
            } 
 
            else
            {
                Iterator<ErrorObject> errorList =authResponseBean.getAuthErrorList().iterator();
                while (errorList.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList.next();
                    response.setError(errorObject.getErrorCode(), errorObject.getErrorDescription());
                }
            }
        } 
        catch (Exception e) 
        {
            response.codigo="0666";
            response.mensaje= "**Check Log for more details:" + e.getMessage();
            logger.error("Error: {}", e);
        }        
        return response;
    }
    
    
    private boolean verifyCardNumber(String cardNumber)
    {
        boolean isValid= false;
        String s = cardNumber;
        char checkDigit=' ';
        char[] array;
        int odds = 0,evens = 0, total=0, iCheckDigit=0;
        String msg="";
        int i=0;
        
        //reverse
        StringBuffer sb = new StringBuffer(s);
        sb = sb.reverse();
        s = sb.toString();

        //To Array
        array = s.toCharArray();
        
        if(array.length==15)
        {
            //Get checkDigit
            checkDigit = array[0];
            iCheckDigit = Character.digit(checkDigit, 10);
            
            for(int x=0; x<array.length;x++)
                msg+=array[x]+", ";
            
            //Odds
            for(i=1;i<array.length;i+=2)
            {
                int j = 0;
                j = Character.digit(array[i], 10);
                j = j*2;
                if(j>9)
                {
                    char[] n = (j+"").toCharArray();
                    j = Character.digit(n[0], 10) + Character.digit(n[1], 10);
                }
                odds += j;
            }
            
            //Even
            for(i=0;i<array.length;i+=2)
            {
                int j = 0;
                j = Character.digit(array[i], 10);
                evens += j;
            }
        }
        
        //Suma
        total = odds + evens;
        
        if(total%10==0 || total%10==iCheckDigit)
            isValid = true;
        else
            isValid = false;

        return isValid;
    }
    
    private void swapExpirationDate()
    {
        String par1 = "";
        String par2 = "";
                
        String anio = "";
        String mes = "";
        
        par1 = cardExpirationDate.substring(0, 2);
        par2 = cardExpirationDate.substring(2, 4);
        
        if(Integer.parseInt(par1)>12)
        {
            anio = par1;
            mes = par2;
        }
        
        if(Integer.parseInt(par2)>12)
        {
            mes = par1;
            anio = par2;
        }
        
        cardExpirationDate = anio + mes;
    }
    
    
    private static String getLocalDateTime(String format) {
            return now(format);
    }
    
    private static String now(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());
    }
    
    public static String padLeft(long source, char charToPad, int number)
    {
        return String.format("%"+number+"s", Long.toString(source)).replace(' ', charToPad);
    }
    
    public static String padLeft(double source, char charToPad, int number)
    {
        return String.format("%"+number+"s", Double.toString(source)).replace(' ', charToPad);
    }
    
    private String setTransactionId(long i)
    {
        String s = "";
        s = padLeft(i, '0', 6);
        return s;
    }    
    
    private String setAmount(double i)
    {
        String numero = "";
        String decimales = "00";
        
        String number = Double.toString(i);
        String[] divs = number.split("\\.");
        if(divs!=null && divs.length>1 )
        {
            long l = Long.parseLong(divs[0]);
            numero = padLeft(l, '0', 10);
            decimales = divs[1];
            if(decimales.length()==1)
                decimales = decimales +"0";
                
            numero = numero + decimales;
        }
        
        return numero;
    }    

}
