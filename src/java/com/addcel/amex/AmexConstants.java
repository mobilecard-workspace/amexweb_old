/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.amex;

import com.americanexpress.ips.enums.CountryCode;
import com.americanexpress.ips.enums.CurrencyCode;
import com.americanexpress.ips.enums.FunctionCode;
import com.americanexpress.ips.enums.MCCCode;
import com.americanexpress.ips.enums.MessageReasonCode;

/**
 *
 * @author ADDCEL13
 */
public class AmexConstants {
	
	public final static String AMBIENTE_QA="QA";
    public final static String AMBIENTE_PROD="PROD";
    public final static String AMBIENTE_ACTUAL="PROD";
    //public final static String AMBIENTE_ACTUAL="PROD";
    public static Integer NUMERO_AUTORIZACION=((int)(Math.random()*999000));
    
    //Common
    public static String AMEX_APPROVAL_CODE_LENGHT ="6";
    public static String AMEX_TERMINAL ="00000001";
    public static String AMEX_ACCEPTOR_ID ="9351358974";
    public static String AMEX_COUNTRY_CODE =CountryCode.Mexico.getCountryCode();;
    public static String AMEX_POS ="1009S0S00000";//"1000S0S00000";
    public static String AMEX_CURRENCY_CODE =CurrencyCode.Mexican_Peso.getCurrencyCode();
    public static String AMEX_ACQUIRER_REFERENCE_DATA ="000071169033272";
    public static String AMEX_FORWARD_INSTITUTION_ID_CODE ="\\";
    
    
    //Third Party Constants
    public static String AMEX_TPP_NAME ="S#ADDCEL.COM";
    public static String AMEX_TPP_STREET ="PASEO DE LA ASUNCION";
    public static String AMEX_TPP_CITY ="MEXICO";
    public static String AMEX_TPP_POSTALCODE ="52149";
    
    //Reversal 1420
    public static String AMEX_REVERSAL_MESSAGE_TYPE ="1420";
    public static String AMEX_REVERSAL_MESSAGE_REASON ="1400";
    public static String AMEX_REVERSAL_PROCESSING_CODE ="024000";
    public static String AMEX_REVERSAL_FUNCTION_CODE =FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
    public static String AMEX_REVERSAL_APPROVAL_LENGHT_CODE ="6";
    public static String AMEX_REVERSAL_SECONDARY_KEY ="ITD";
    public static String AMEX_REVERSAL_CARD_ACCEPTOR_BUSINESS =MCCCode.Miscellaneous_General_Merchandise.getMccCode();
    public static String AMEX_REVERSAL_CUSTOMER_EMAIL ="";
    public static String AMEX_REVERSAL_CUSTOMER_HOST ="mobilecard.mx";
    public static String AMEX_REVERSAL_CUSTOMER_IP ="50.57.192.210";
    public static String AMEX_REVERSAL_CUSTOMER_INFO_ID ="61";
    public static String AMEX_REVERSAL_ACQUIRING_INST_ID_ORIGINAL ="109351358974";
        
    
    //1100
    public static String AMEX_AUTHORIZATION_AAV_MESSAGE_TYPE ="1100";
    public static String AMEX_AUTHORIZATION_AAV_PROCESSING_CODE ="004800";
    public static String AMEX_AUTHORIZATION_AAV_FUNCTION_CODE = FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();//FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
    public static String AMEX_AUTHORIZATION_AAV_ACCEPTOR_BUSINESS = MCCCode.Miscellaneous_General_Merchandise.getMccCode();
    public static String AMEX_AUTHORIZATION_AAV_ITD_SECONDARY_KEY = "ITD";
    public static String AMEX_AUTHORIZATION_AAV_ITD_CUSTOMER_HOST = "mobilecard.mx";
    public static String AMEX_AUTHORIZATION_AAV_ITD_CUSTOMER_IP = "50.57.192.210";
    public static String AMEX_AUTHORIZATION_AAV_SHIPPING_TO_COUNTRY = "484";
    public static String AMEX_AUTHORIZATION_AAV_SHIPPING_METHOD = "02";
    public static String AMEX_AUTHORIZATION_AAV_INFO_IDENTIFIER = "61";
    public static String AMEX_AUTHORIZATION_AAV_MESSAGE_IDENTIFIER = MessageReasonCode.value1.getMessageReasonCode();
    public static String AMEX_AUTHORIZATION_AAV_APPROVAL_CODE_IDENTIFIER = MessageReasonCode.value1.getMessageReasonCode();
    public static String AMEX_AUTHORIZATION_AAV_APPROVAL_CODE_LENGHT = "6";
    
    
    public static String AMEX_AUTHORIZATION_AAV_CM_SERVICE_IDENTIFIER = "AX";
    public static String AMEX_AUTHORIZATION_AAV_CM_REQUEST_TYPE_IDENTIFIER_3378 = "AD";
    public static String AMEX_AUTHORIZATION_AAV_CM_REQUEST_TYPE_IDENTIFIER = "AE";
    public static String AMEX_AUTHORIZATION_AAV_CM_SHIP_TO_COUNTRY_CODE = "484";
    
    public static String AMEX_AUTHORIZATION_AAV_ACQUIRER_INSTITUTION_IDENTIFICATION_CODE = "";//"9351358974";
    
}
