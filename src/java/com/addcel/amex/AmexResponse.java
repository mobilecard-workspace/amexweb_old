/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.amex;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADDCEL13
 */
public class AmexResponse {
    private List<AmexResponse> errores;
    public String transactionId;
    public String codigo;
    public String mensaje;
    private String accReference;
    private String approvalCode;
    
    public AmexResponse()
    {
        errores = new ArrayList();
    }
    
    public void setError(String codigo, String mensaje)
    {
        AmexResponse error = new AmexResponse();
        error.codigo = codigo;
        error.mensaje = mensaje;
        errores.add(error);
    }
    
    public List getErrores()
    {
        return errores;
    }
    
    public AmexResponse getRespuesta()
    {
        AmexResponse respuesta = new AmexResponse();
        respuesta.transactionId = transactionId;
        respuesta.codigo = codigo;
        respuesta.mensaje = mensaje;
        return respuesta;
    }
    
    public Boolean hasErrors()
    {
        if(errores==null)
            return false;
        else if(errores!=null && errores.isEmpty())
            return false;
        else
            return true;
    }

    /**
     * @return the accReference
     */
    public String getAccReference() {
        return accReference;
    }

    /**
     * @param accReference the accReference to set
     */
    public void setAccReference(String accReference) {
        this.accReference = accReference;
    }

    /**
     * @return the approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * @param approvalCode the approvalCode to set
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }
}
