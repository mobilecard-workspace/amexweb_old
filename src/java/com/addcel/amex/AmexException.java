/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.amex;

/**
 *
 * @author ADDCEL13
 */
public class AmexException  extends Exception
{
    private String key;
    private String message;
    public AmexException(String message)
    {
        super(message);
    }
        
    public AmexException(String key, String message)
    {
        super("AmexException "+key+": "+message);
    }
    
    @Override
    public String getMessage()
    {
        return "AmexException "+key+": "+message;
    }
}
