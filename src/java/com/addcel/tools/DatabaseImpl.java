package com.addcel.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ADDCEL13
 */
public class DatabaseImpl {
	
	private static final Logger logger = LoggerFactory.getLogger(DatabaseImpl.class);
	
    private Connection  connection  = null;
//    private String userName = "mobilcard";//root-mobilcard
//    private String password = "Mobil1696Card";// 54V173?Fo0nt-mobilcard
//    private String url = "jdbc:mysql://DataBaseMC/mobilcard";//addcel-mobilcard
    
    private String userName = "ecommerce";//root-mobilcard
    private String password = "AddcellWeb2012";// 54V173?Fo0nt-mobilcard
    private String url = "jdbc:mysql://DataBaseMC/ecommerce";//addcel-mobilcard

    
//    private String userName = "root";//root-mobilcard
//    private String password = "sistemas";// 54V173?Fo0nt-mobilcard
//    private String url = "jdbc:mysql://localhost/ecommerce";//addcel-mobilcard
    
    public DatabaseImpl()
    {
        
    }
    
    public DatabaseImpl(String dataBase)
    {
        if(dataBase.equals("mobilecard"))
        {
            userName = "mobilcard";
            password="Mobil1696Card";
            url = "jdbc:mysql://DataBaseMC/"+dataBase;
        }
    }

    public  Connection getConnection() throws ClassNotFoundException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        //connection = DriverManager.getConnection(url, userName, password);
        return DriverManager.getConnection(url, userName, password);
    }

    public void disconect() throws SQLException{
        connection.close();
    }    

    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * ResultSet, PreparedStatement, connection
     * @param preSt
     * @param res
     * @throws SQLException 
     */
    public void closeConPrepStResSet(PreparedStatement preSt,ResultSet res, Connection conn){
        try{
            closeResultSet(res);
            closePreparedStatement(preSt);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPrepStResSet : General Exception: {}", e);            
        }
    }
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * ResultSet, PreparedStatement, connection
     * @param preSt
     * @param res
     * @throws SQLException 
     */
    public void closeConPrepStResSet(Statement st,ResultSet res, Connection conn){
        try{
            closeResultSet(res);
            closeStatement(st);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPrepStResSet : General Exception: {}", e);            
        }
    }
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * PreparedStatement, connection
     * @param preSt
     * @throws SQLException 
     */
    public void closeConPreparedSt(PreparedStatement preSt, Connection conn){
        try{
            closePreparedStatement(preSt);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPreparedSt : General Exception: {}", e);            
        }
    }
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * PreparedStatement, connection
     * @param preSt
     * @throws SQLException 
     */
    public void closeConSt(Statement preSt, Connection conn){
        try{
            closeStatement(preSt);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPreparedSt : General Exception: {}", e);            
        }
    }
    
     /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * ResultSet, connection
     * @param res
     * @throws SQLException 
     */
    public void closeConResultSet(ResultSet res, Connection conn){
        try{
            closeResultSet(res);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConResultSet : General Exception: {}", e);            
        }
    }
    
    public void closeConnection(Connection conn) {
        try{
            if(conn != null){
                conn.close();
            }
        }catch(SQLException e){
            logger.error("Error closeConnection : General Exception: {}", e);            
        }
    }
    
    private void closePreparedStatement(PreparedStatement preSt)throws SQLException{
        try{
            if(preSt != null){
                preSt.close();
            }
        }catch(SQLException e){
            logger.error("Error closePreparedStatement : General Exception: {}", e);            
        }
    }
    
    private void closeStatement(Statement preSt)throws SQLException{
        try{
            if(preSt != null){
                preSt.close();
            }
        }catch(SQLException e){
            logger.error("Error closeStatement : General Exception: {}", e);            
        }
    }
    
    private void closeResultSet(ResultSet res)throws SQLException{
        try{
            if(res != null){
                res.close();
            }
        }catch(SQLException e){
            logger.error("Error closeResultSet : General Exception: {}", e);            
        }
    }

    
}
