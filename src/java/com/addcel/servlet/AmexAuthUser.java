/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.servlet;

import com.addcel.amex.AmexData;
import com.addcel.amex.AmexResponse;
import com.addcel.amex.AmexTransaction;
import com.addcel.amex.TransactionControl;
import com.addcel.common.MCUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Salvador
 */
public class AmexAuthUser extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            System.out.println("AmexAuthorization:init...");
            java.util.Date date = new java.util.Date(); 
            java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("dd/MM/yyyy");
            String fecha = sdf.format(date);            
            
            
            AmexTransaction amex = new AmexTransaction();
            
            MCUser mcUser = new MCUser();
            mcUser = TransactionControl.getUserData("user");
            
            
            String tarjeta = request.getParameter("tarjeta");
            String expira = request.getParameter("vigencia");
            String monto = request.getParameter("monto");
            String cid = request.getParameter("cid"); //cvv2
            String producto = request.getParameter("producto"); //hardcode, otra vez, malhabido y horrible
                                    
            
            String afiliacion = "";
            if(producto==null || producto.trim().equals(""))
                afiliacion = "9351358974";
            else if(producto.trim().equals("IAVE"))
                afiliacion = "9351669578";
            else if(producto.trim().equals("VIAPASS"))
                afiliacion = "9351669081";
            else if(producto.trim().equals("OHL"))
                afiliacion = "9351669032";
                
                        
            System.out.println("AmexAuthorization:Parametros..." + tarjeta+"."+expira+"."+monto+"."+cid );
            

            String direccion = request.getParameter("direccion");
            String cp = request.getParameter("cp");
            
            String email = request.getParameter("email");
            String nombres = request.getParameter("nombres");
            String apellidos = request.getParameter("apellidos");
            String telefono = request.getParameter("telefono");
                        
            //Por ahora poner la direccion fija
            direccion = "";
            
            if(direccion==null) direccion = "";
            if(cp==null) cp = "";
            if(email==null) email = "";
            if(nombres==null) nombres = "";
            if(apellidos==null) apellidos = "";
            if(telefono==null) telefono = "";
            
            if(tarjeta != null && !tarjeta.equals("") && expira!=null && !expira.equals("") )
            {
            
                System.out.println("AmexAuthorization:Acepta parametros...");
                String processingCode = "004800";

                String service = "AX";
                String type = "AE";    


                String shipToCP = "";
                String shipToDireccion = "";
                String shipToNombres = "";
                String shipToApellidos = "";
                String shipToTelefono = "";

                String paymentType = "";
                String installations = "";
                String dpp = "";

                double fee = 0l;

                AmexResponse respuesta = null;

                if(monto!=null)
                    fee = Double.parseDouble(monto);

                String s="";

                //Algo pasa con la fecha
                String anio = expira.trim().substring(0, 2);
                String mes = expira.trim().substring(2,4);
                
                int iAnio = Integer.parseInt(anio);
                if(iAnio<=12)
                {
                    expira = mes + anio;
                }

                AmexData datos = new AmexData();
                datos.setAcceptorId(afiliacion);
                datos.setProcessingCode(processingCode);
                datos.setPrimaryAccountNumber(tarjeta);
                datos.setCardExpirationDate(expira);
                datos.setNumberAmount(fee);
                datos.setCid(cid);
                datos.setBillingEmail(email);
                datos.setBillingCP(cp);
                datos.setBillingDireccion(direccion);
                datos.setBillingNombre(nombres);
                datos.setBillingApellido(apellidos);
                datos.setBillingTel(telefono);

                datos.setShipToCP(shipToCP);
                datos.setShipToDireccion(shipToDireccion);
                datos.setShipToNombre(shipToNombres);
                datos.setShipToApellido(shipToApellidos);
                datos.setShipToTel(shipToTelefono);

                datos.setServiceIdentifier(service);
                datos.setTypeIdentifier(type);
                datos.setPaymentType(paymentType);
                datos.setInstallations(installations);
                datos.setDpp(dpp);


                //respuesta = amex.cardAuthorizationRequestAAV(tarjeta, expira, fee, cid, email, cp, direccion, nombres, apellidos, telefono);
                System.out.println("AmexAuthorization:Realiza transaccion...");
                respuesta = amex.cardAuthorizationRequestAAV(datos);

                s = "{\"transaction\":\""+respuesta.transactionId+"\",\"code\":\""+respuesta.codigo+"\",\"dsc\":\""+respuesta.mensaje+"\"";

                if(respuesta.hasErrors())
                {
                    List errores = respuesta.getErrores();
                    for(int i=0; i<errores.size();i++)
                    {
                        AmexResponse error = (AmexResponse)errores.get(i);
                        s +=",\"error\":\""+error.codigo+"\",\"errorDsc\":\""+error.mensaje+"\"";
                    }
                }
                s+="}";
                response.getWriter().println(s);
            }
            else
            {
                System.out.println("AmexAuthorization:Parametros incorrectos...");
                response.getWriter().println("{[]}");
            }
            
            
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        } 
        finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
