package AmexTest;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.addcel.amex.AmexException;
import com.americanexpress.ips.AuthorizationService;
//import com.americanexpress.ips.authorization.bean.AdditionalDataNationalBean;
//import com.americanexpress.ips.authorization.bean.AuthorizationRequestBean;
//import com.americanexpress.ips.authorization.bean.AuthorizationResponseBean;
//import com.americanexpress.ips.authorization.bean.CardAcceptorNameLocationBean;
//import com.americanexpress.ips.authorization.bean.ErrorObject;
//import com.americanexpress.ips.authorization.bean.OriginalDataElementsBean;
//import com.americanexpress.ips.authorization.bean.PrivateUseData2Bean;
import com.americanexpress.ips.connection.PropertyReader;
import com.americanexpress.ips.enums.CountryCode;
import com.americanexpress.ips.enums.CurrencyCode;
import com.americanexpress.ips.enums.FunctionCode;
import com.americanexpress.ips.enums.MCCCode;
import com.americanexpress.ips.enums.MessageReasonCode;
import com.americanexpress.ips.gcag.bean.AdditionalDataNationalBean;
import com.americanexpress.ips.gcag.bean.AuthorizationRequestBean;
import com.americanexpress.ips.gcag.bean.AuthorizationResponseBean;
import com.americanexpress.ips.gcag.bean.CardAcceptorNameLocationBean;
import com.americanexpress.ips.gcag.bean.ErrorObject;
import com.americanexpress.ips.gcag.bean.OriginalDataElementsBean;
import com.americanexpress.ips.gcag.bean.PrivateUseData2Bean;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

/**
 *
 * @author ADDCEL13
 */
public class AmexTest {
    
    
    //Field 1 Message Type 
    //1100 Authorization    
    private String messageType = "";
    
    //Field 2 Primary Account Number (PAN) (Agregar "15" antes de
    private String primaryAccountNumber = "";
    
    //Field 3 Processing Code
    /*
     * 004000 = Card Authorization Request
     * 004800 = Combination Automated Address Verification (AAV) and Authorization
     * 034000 = AMEX Emergency Check Cashing
     * 064000 = AMEX Travelers Cheque Encashment
     * 174800 = Transaction for Automated Address Verification (AAV) Only
     */
    private String processingCode="";
    
    //Field 4 Amount
    /*
     * For example, for US Dollar (840) transactions, two decimal places are implied. 
     * Thus, the value $100.00 would be entered as:
     * "000000010000"
     */
    private String amount="";
    
    //Field 7 Date and Time
    private String yymmdd="";
    private String hhmmss="";			
    private String yymmddhhmmss="";	
    
    //Field 11 - Unique transaction Id
    private String transactionId = "";
    
    //Field 14 - Expiration Card Date YYMM
    private String cardExpirationDate = ""; 
    
    //Field 19 - Country Code
    private String countryCode = "";
    
    //Field 22 - POS Point of Service Code - 1000S0100130 1000S0100000
    private String pos = "";
    
    //Field 24 - Function Code
    /*
     * 100 - Authorization Request
     * 108 - Authorization Inquiry Request
     * 180 - Batch Authorization
     * 181 - Prepaid Card Partial Authorization Supported
     * 182 - Prepaid Card Authorization with Balance Return Sup-ported
     * 831 - Prepaid Card Authorization with Balance Return Sup-ported
     */
    private String functionCode = "";
    
    /*
     * Field 25 - Message Reason Code
     * 
     * messageReason1->1900
     * messageReason2->1400
     * messageReason3->8700    
     * 
     */
    private String messageReason="";
    
    //Field 26 - Card Acceptor Business Code
    /*
     * 5045 - Computers, Computer Peripheral Equipment, and Software
     * 4829 - Wire Transfers and Money Orders
     * 5046 - Commercial Equipment - Not Elsewhere Classified
     */
    private String cardAcceptorBusiness="";
    
    //Field 27 - Approval Code Lenght : 6 or 2
    private String approvalCodeLenght="";
    
    //Field 31 - Acquirer Reference "Data UNUSED"
    private String acquirerReferenceData = "000071169033272";
    
    //Field 32 - Acquirer Institution Identification Code : \
    private String acquirerInstitutionIdentificationCode = "";
            
    //Field 33 - FORWARDING INSTITUTION IDENTIFICATION CODE NOt Present
    private String forwardInstitutionIdCode = "";
    
    //Field 35 - Track Data 2 Not used, Card Not Present
    
    //Field 37 - Retrieval Reference Number : Transaction number recomended
    private String retrievalReferenceNumber = "";
    
    //Field 41 - Card Acceptor Terminal Identificator: Recomended System Id (MobileCard, MobileWallet, PayBack)
    private String cardAcceptorTerminalId = "";
            
    //Field 42 - Card Acceptor Identification Code: Merchant ID. 
    private String cardAcceptorId = "";
    
    //Field 43 - Card Acceptor Name/Location
    private String tppName = "";
    private String tppStreet = "";
    private String tppCity = "";
    private String tppPostalCode = "";
    
    //Field 45 - Track 1 Data: No Aplica
    
    //Field 47 - Additional Data - National
    private String itdSecondaryKey="";
    private String itdCustomerEmail="";
    private String itdCustomerHostName="";
    private String itdHttpBrowserType="";
    private String itdShipToCountry="";
    private String itdShippingMethod="";
    private String itdMerchantProductSku="";
    private String itdCustomerIP="";
    private String itdCustomerANI="";
    private String itdCustomerInfoIdentifier="";
    
    //Field 48 - Additional Data - Private : Not Needed
    
    //Field 49 - Currency Code
    private String currencyCode="";
    
    //Field 52 Personal Identification Data: Not Used
    
    //Field 53 Security Related Control Information: CID
    private String cardIdentifierCode="";
    
    //Field 55 Integrated Circuit Card System Related Data
    
    //Field 60 National Use Data - Not Used
    
    //Field 61 National Use Data - Not Used
    
    //Field 62 Private use data - Not Used
    
    //Field 63 Private use data - Optional for AAV
    //** ATENCION ESTE SE USA CON AAV 004800 Y 174800
    private String cmServiceIdentifier = "";
    private String cmRequestTypeIdentifier = "";
    private String cmBillingName = "";
    private String cmBillingFirstName = "";
    private String cmBillingLastName = "";
    private String cmBillingPostalCode = "";
    private String cmBillingAdress = "";
    private String cmBillingPhoneNumber = "";
    private String cmShipToPostalCode = "";
    private String cmShipToAddress = "";
    private String cmShipToName = "";
    private String cmShipToFirstName = "";
    private String cmShipToLastName = "";
    private String cmShipToPhone = "";
    private String cmShipToCountryCode = "";
    
    
    
    public static void main(String[] args)
    {
        Boolean isValid = false;
        String cardNumber="373732134561008";
        String expiration ="1506"; //YYMM Junio del 2015
        double amount = 1000.10d;
        String transactionId = "000001";
        String email = "sburgara@gmail.com";
        
        
        //AmexTest test = new AmexTest(transactionId,cardNumber, expiration,amount, email);
    }
    
    
    public AmexTest(long transactionId)
    {   
        this.transactionId = setTransactionId(transactionId);
        System.out.println("######this.transactionId="+this.transactionId);
        yymmdd=getLocalDateTime("yyMMdd");
        hhmmss=getLocalDateTime("hhmmss");			
        yymmddhhmmss=yymmdd+hhmmss;	
        
        
        //Todo esto se toma de la base de datos k
        //Third Party Processor data
        //tppName = "RADIOCOMUNICACIONES Y SOLUCIONES CELULARES";
        tppName = "S#ADDCEL.COM";
        tppStreet = "PASEO DE LA ASUNCION";
        tppCity = "MEXICO";
        tppPostalCode = "52149";
        
        approvalCodeLenght="6";
        retrievalReferenceNumber = this.transactionId;
        cardAcceptorTerminalId = "1"; //MobileCard
        cardAcceptorId = "9351358974";    //Asignado por AMEX
        countryCode = CountryCode.Mexico.getCountryCode();
        pos="1000S0S00000"; //"1000S0100000"
        
        currencyCode = CurrencyCode.Mexican_Peso.getCurrencyCode();
        acquirerReferenceData = "000071169033272";
        
        forwardInstitutionIdCode = "\\";
    }
        
    
    public String cardAuthorizationRequest() throws AmexException            
    {
//        messageType = "0100";
//        processingCode = "004000";
//        cardAcceptorId = "9351358974";    //Asignado por AMEX
//        
//        
//        //Verificar tarjeta
//        if(!verifyCardNumber(primaryAccountNumber))
//            throw new AmexException("1001","Invalid Card Number");
//        
//        sendRequest();
        
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        //Datos específicos para 1100 Authorization Request Message
        //004000
        
        //Data Test from AMEX Test Script MX-BA-484-?-001
        messageType = "1100";
        processingCode = "004000";  //Card Authorization Request
        primaryAccountNumber = "376677849952008";
        cardExpirationDate = "1211";        
        functionCode = FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
        amount="000000001600";
        cardAcceptorBusiness = MCCCode.Miscellaneous_General_Merchandise.getMccCode();
        cardIdentifierCode = "2234";
        
        
        
        itdSecondaryKey="ITD";
        itdCustomerEmail="JLOPEZ@CLEARCHANNEL.COM";
        itdCustomerHostName="mobilecard.mx";
        itdHttpBrowserType="";
        itdShipToCountry="484";            //ISO 3166
        itdShippingMethod="02";             //??
        itdMerchantProductSku="";
        itdCustomerIP="50.57.192.210";
        itdCustomerANI="";
        itdCustomerInfoIdentifier="61";        
        
        messageReason = MessageReasonCode.value1.getMessageReasonCode();
        approvalCodeLenght="6";
        
        //Information Verification - No Disponible
        
                
            
        authRequestBean.setMessageTypeIdentifier(messageType);
        authRequestBean.setProcessingCode(processingCode);
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        authRequestBean.setFunctionCode(functionCode);
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        authRequestBean.setAmountTransaction(amount);	
        authRequestBean.setPosDataCode(pos);
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        authRequestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId);
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId);
        authRequestBean.setTransactionCurrencyCode(currencyCode);	

        //Card Acceptor Name/Location #
        CardAcceptorNameLocationBean cardAcceptNameLocationBean = new CardAcceptorNameLocationBean();
        
        cardAcceptNameLocationBean.setSubfield1Name(tppName);
        cardAcceptNameLocationBean.setSubfield1Street(tppStreet);
        cardAcceptNameLocationBean.setSubfield1City(tppCity);
        cardAcceptNameLocationBean.setSubfield2PostalCode(tppPostalCode);
                
        authRequestBean.setCardAcceptorNameLocation(cardAcceptNameLocationBean.populateCardAccpetorNameLocation(authResponseBean));
        
        //Additional Data National
        AdditionalDataNationalBean additionalDataNationalBean = new AdditionalDataNationalBean();

        additionalDataNationalBean.setSecondaryId(itdSecondaryKey);
        additionalDataNationalBean.setCustomerEmail(itdCustomerEmail);
        additionalDataNationalBean.setCustomerHostname(itdCustomerHostName);
        additionalDataNationalBean.setHttpBrowserType(itdHttpBrowserType);
        additionalDataNationalBean.setShipToCountry(itdShipToCountry);
        additionalDataNationalBean.setShippingMethod(itdShippingMethod);
        additionalDataNationalBean.setMerchantProductSku(itdMerchantProductSku);
        additionalDataNationalBean.setCustomerIP(itdCustomerIP);
        additionalDataNationalBean.setCustomerANI(itdCustomerANI);
        
        //CUSTOMER II DIGITS
        //additionalDataNationalBean.setCustomerInfoIdentifier(itdCustomerInfoIdentifier);
        authRequestBean.setAdditionalDataNational(additionalDataNationalBean.populateAdditionalDataNational(authResponseBean));
        
        
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        
        authRequestBean.setMessageReasonCode(messageReason);
        authRequestBean.setApprovalCodeLength(approvalCodeLenght);
        
        
        
        authRequestBean.setCardIdentifierCode(cardIdentifierCode);
        
        
                        
        return sendRequestSimple(authRequestBean, authResponseBean);        
    }
    
    public String cardReversalAdviceRequest()  throws AmexException
    {
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
                
        //Datos específicos para 1420 Authorization Request Message        
        messageType = "1420";
        primaryAccountNumber = "376677849952008";
        processingCode = "024000";  //Card Authorization Request
        amount="000000001600";
        //trace
        //datetime
        cardExpirationDate = "1211";        
        //country code
        
        functionCode = FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
        cardAcceptorBusiness = MCCCode.Miscellaneous_General_Merchandise.getMccCode();
        cardIdentifierCode = "2234";
        
        
        
        itdSecondaryKey="ITD";
        itdCustomerEmail="JLOPEZ@CLEARCHANNEL.COM";
        itdCustomerHostName="mobilecard.mx";
        itdHttpBrowserType="";
        itdShipToCountry="484";            //ISO 3166
        itdShippingMethod="02";             //??
        itdMerchantProductSku="";
        itdCustomerIP="50.57.192.210";
        itdCustomerANI="";
        itdCustomerInfoIdentifier="61";        
        
        messageReason = "1400";//MessageReasonCode.value3.getMessageReasonCode();
        approvalCodeLenght="6";
        
        //Information Verification - No Disponible                        
        
        //Field 1
        authRequestBean.setMessageTypeIdentifier(messageType);
        //Field 2
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        //Field 3
        authRequestBean.setProcessingCode(processingCode);
        //Field 4
        authRequestBean.setAmountTransaction(amount);	
        //Field 11
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        //Field 12
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        //Field 14
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        //Field 19
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        //Field 22
        authRequestBean.setPosDataCode(pos);
        //Field 25
        authRequestBean.setMessageReasonCode(messageReason);
        //Field 26
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        //Field 31 NOT USED
        //Field 31
        authRequestBean.setTransactionId(acquirerReferenceData);
        
        //Field 42
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId);
        //Field 49
        authRequestBean.setTransactionCurrencyCode(currencyCode);	
        
                
        //Field 56
        //Original Data
        OriginalDataElementsBean orignalDataElementsBean = new OriginalDataElementsBean();
        //Field 1
        orignalDataElementsBean.setMessageTypeIdOriginal("1100");
        //Field 2
        orignalDataElementsBean.setSystemTraceAuditNumberOriginal("000005");
        //Field 3
        orignalDataElementsBean.setLocalTransactionDateAndTimeOriginal("120606055801");
        //Field 4
        orignalDataElementsBean.setAcquiringInstIdOriginal("109351358974"); //cardAcceptorId        
        authRequestBean.setOriginalDataElementsBean(orignalDataElementsBean);
        
        
        
                
//        authRequestBean.setFunctionCode(functionCode);
//        authRequestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId);
//
//        //Card Acceptor Name/Location #
//        CardAcceptorNameLocationBean cardAcceptNameLocationBean = new CardAcceptorNameLocationBean();
//        
//        cardAcceptNameLocationBean.setSubfield1Name(tppName);
//        cardAcceptNameLocationBean.setSubfield1Street(tppStreet);
//        cardAcceptNameLocationBean.setSubfield1City(tppCity);
//        cardAcceptNameLocationBean.setSubfield2PostalCode(tppPostalCode);
//                
//        authRequestBean.setCardAcceptorNameLocation(cardAcceptNameLocationBean.populateCardAccpetorNameLocation(authResponseBean));
//        
//        //Additional Data National
//        AdditionalDataNationalBean additionalDataNationalBean = new AdditionalDataNationalBean();
//
//        additionalDataNationalBean.setSecondaryId(itdSecondaryKey);
//        additionalDataNationalBean.setCustomerEmail(itdCustomerEmail);
//        additionalDataNationalBean.setCustomerHostname(itdCustomerHostName);
//        additionalDataNationalBean.setHttpBrowserType(itdHttpBrowserType);
//        additionalDataNationalBean.setShipToCountry(itdShipToCountry);
//        additionalDataNationalBean.setShippingMethod(itdShippingMethod);
//        additionalDataNationalBean.setMerchantProductSku(itdMerchantProductSku);
//        additionalDataNationalBean.setCustomerIP(itdCustomerIP);
//        additionalDataNationalBean.setCustomerANI(itdCustomerANI);
//        
//        //CUSTOMER II DIGITS
//        //additionalDataNationalBean.setCustomerInfoIdentifier(itdCustomerInfoIdentifier);
//        authRequestBean.setAdditionalDataNational(additionalDataNationalBean.populateAdditionalDataNational(authResponseBean));
//        
//        
//        
//        authRequestBean.setApprovalCodeLength(approvalCodeLenght);
//        
//        
//        //CID
//        authRequestBean.setCardIdentifierCode(cardIdentifierCode);          
                                       
        return sendRequestSimple(authRequestBean, authResponseBean);        
        
    }

    public String cardAuthorizationRequestAAV() throws AmexException
    {
//        //004800
//        messageType = "0100";
//        processingCode = "004800";
//        cardAcceptorId = "9351358974";    //Asignado por AMEX
//        
//        
//        //Verificar tarjeta
//        if(!verifyCardNumber(primaryAccountNumber))
//            throw new AmexException("1001","Invalid Card Number");
//        
//        /**SI ES AAV**/
//        cmServiceIdentifier = "AX";
//        cmRequestTypeIdentifier = "AD";
//        cmBillingPostalCode = postalCode;
//        cmBillingAdress = address;
        
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        //Datos específicos para 1100 Authorization Request Message
        //004000
        
        //Data Test from AMEX Test Script MX-BA-484-?-001
        messageType = "1100";
        processingCode = "004800";  //Card Authorization Request
        primaryAccountNumber = "376677849952008";
        cardExpirationDate = "1211";        
        functionCode = FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode();
        amount="000000001600";
        cardAcceptorBusiness = MCCCode.Miscellaneous_General_Merchandise.getMccCode();
        cardIdentifierCode = "2234";
        
        
        
        itdSecondaryKey="ITD";
        itdCustomerEmail="JLOPEZ@CLEARCHANNEL.COM";
        itdCustomerHostName="mobilecard.mx";
        itdHttpBrowserType="";
        itdShipToCountry="484";            //ISO 3166
        itdShippingMethod="02";             //??
        itdMerchantProductSku="";
        itdCustomerIP="50.57.192.210";
        itdCustomerANI="";
        itdCustomerInfoIdentifier="61";        
        
        messageReason = MessageReasonCode.value1.getMessageReasonCode();
        approvalCodeLenght="6";

        //AAV
        cmServiceIdentifier = "AX";
        cmRequestTypeIdentifier = "AE";//"AD";
        cmBillingPostalCode = "06600";
        cmBillingAdress = "REFORMA 350 PISO 16";
        cmBillingName = "JUAN LOPEZ";
        cmBillingFirstName = "JUAN";
        cmBillingLastName = "LOPEZ";
        cmBillingPhoneNumber = "5520944255";
        cmShipToPostalCode = "06600";
        cmShipToAddress = "REFORMA 350 PISO 16 COL JUAREZ";
        cmShipToName = "JUAN LOPEZ";
        cmShipToPhone = "";
        cmShipToCountryCode = "484";
        
        
        acquirerInstitutionIdentificationCode = "9351358974";
        //acquirerInstitutionIdentificationCode = "\\";
        //Information Verification - No Disponible
        
                
            
        authRequestBean.setMessageTypeIdentifier(messageType);
        authRequestBean.setProcessingCode(processingCode);
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        authRequestBean.setFunctionCode(functionCode);
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        authRequestBean.setAmountTransaction(amount);	
        authRequestBean.setPosDataCode(pos);
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        //authRequestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId);
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId);
        authRequestBean.setTransactionCurrencyCode(currencyCode);	

        //Card Acceptor Name/Location #
        CardAcceptorNameLocationBean cardAcceptNameLocationBean = new CardAcceptorNameLocationBean();
        
        cardAcceptNameLocationBean.setSubfield1Name(tppName);
        cardAcceptNameLocationBean.setSubfield1Street(tppStreet);
        cardAcceptNameLocationBean.setSubfield1City(tppCity);
        cardAcceptNameLocationBean.setSubfield2PostalCode(tppPostalCode);
                
        authRequestBean.setCardAcceptorNameLocation(cardAcceptNameLocationBean.populateCardAccpetorNameLocation(authResponseBean));
        
        //Additional Data National
        AdditionalDataNationalBean additionalDataNationalBean = new AdditionalDataNationalBean();

        additionalDataNationalBean.setSecondaryId(itdSecondaryKey);
        additionalDataNationalBean.setCustomerEmail(itdCustomerEmail);
        additionalDataNationalBean.setCustomerHostname(itdCustomerHostName);
        additionalDataNationalBean.setHttpBrowserType(itdHttpBrowserType);
        additionalDataNationalBean.setShipToCountry(itdShipToCountry);
        additionalDataNationalBean.setShippingMethod(itdShippingMethod);
        additionalDataNationalBean.setMerchantProductSku(itdMerchantProductSku);
        additionalDataNationalBean.setCustomerIP(itdCustomerIP);
        additionalDataNationalBean.setCustomerANI(itdCustomerANI);
        
        //CUSTOMER II DIGITS
        //additionalDataNationalBean.setCustomerInfoIdentifier(itdCustomerInfoIdentifier);
        authRequestBean.setAdditionalDataNational(additionalDataNationalBean.populateAdditionalDataNational(authResponseBean));
        
        
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        
        authRequestBean.setMessageReasonCode(messageReason);
        authRequestBean.setApprovalCodeLength(approvalCodeLenght);                        
        authRequestBean.setCardIdentifierCode(cardIdentifierCode);
        
        authRequestBean.setAcquiringInstitutionIdCode(acquirerInstitutionIdentificationCode);
        
	 PrivateUseData2Bean privateUseData2Bean = new PrivateUseData2Bean();
           //** For 33 byte format - Start
          privateUseData2Bean.setServiceIdentifier(cmServiceIdentifier);
          privateUseData2Bean.setRequestTypeIdentifier(cmRequestTypeIdentifier);
          privateUseData2Bean.setCmBillingPostalcode(cmBillingPostalCode);
          privateUseData2Bean.setCmBillingAddress(cmBillingAdress);
          privateUseData2Bean.setCmFirstName(cmBillingFirstName);
          privateUseData2Bean.setCmLastName(cmBillingLastName);
          privateUseData2Bean.setCmBillingPhoneNumber(cmBillingPhoneNumber);
          privateUseData2Bean.setShipToPostalCode(cmShipToPostalCode);
          privateUseData2Bean.setShipToAddress(cmShipToAddress);
          privateUseData2Bean.setShipToFirstname(cmShipToFirstName);
          privateUseData2Bean.setShipToLastName(cmShipToLastName);
          privateUseData2Bean.setShipToPhoneNumber(cmBillingPhoneNumber);
          privateUseData2Bean.setShipToCountryCode(cmShipToCountryCode);
          //privateUseData2Bean
          //** For 33 byte format - End
                    
          String _res = 
          "\n*************\n" +
          "getMessageTypeIdentifier()->"+authRequestBean.getMessageTypeIdentifier() + "\n" +
          "getSystemTraceAuditNumber()->"+authRequestBean.getSystemTraceAuditNumber() + "\n" +
          "getLocalTransactionDateAndTime()->"+authRequestBean.getLocalTransactionDateAndTime() + "\n" +
          "getAcquiringInstitutionIdCode()->"+authRequestBean.getAcquiringInstitutionIdCode() +  "\n";
//          "getCmBillingPostalcode()->"+privateUseData2Bean.getCmBillingPostalcode() +  "\n" +
//          "getCmBillingAddress()->"+privateUseData2Bean.getCmBillingAddress() +  "\n" +
//          "getCmFirstName()->"+privateUseData2Bean.getCmFirstName() +  "\n" +
//          "getCmLastName()->"+privateUseData2Bean.getCmLastName() +  "\n" +
//          "getCmBillingPhoneNumber()->"+privateUseData2Bean.getCmBillingPhoneNumber() +  "\n" +
//          "getShipToPostalCode()->"+privateUseData2Bean.getShipToPostalCode() +  "\n" +
//          "getShipToAddress()->"+privateUseData2Bean.getShipToAddress() +  "\n" +
//          "getShipToFirstName()->"+privateUseData2Bean.getShipToFirstName() +  "\n" +
//          "getShipToLastName()->"+privateUseData2Bean.getShipToLastName() +  "\n" +
//          "getShipToPhoneNumber()->"+privateUseData2Bean.getShipToPhoneNumber() +  "\n" +
//          "getShipToCountryCode()->"+privateUseData2Bean.getShipToCountryCode();
          
          System.out.println(_res);
          
          authRequestBean.setVerificationInformation(privateUseData2Bean.populatePrivateUseData(authResponseBean));        

                                
        return sendRequestSimple(authRequestBean, authResponseBean);                
        
    }

    public void aav(String postalCode, String adress) throws AmexException
    {
        //004000
        messageType = "0100";
        processingCode = "174800";
        functionCode = FunctionCode.SYSTEM_AUDIT_CONTROL_ECHO_TEST.getFunctionCode();
        amount="000000000000";
        cardAcceptorId = "9351358974";    //Asignado por AMEX
        
        
        //Verificar tarjeta
        if(!verifyCardNumber(primaryAccountNumber))
            throw new AmexException("1001","Invalid Card Number");

        /**SI ES AAV**/
        cmServiceIdentifier = "AX";
        cmRequestTypeIdentifier = "AD";
        cmBillingPostalCode = "";
        cmBillingAdress = "";
        
    }
    
    public String networkManagementRequest() throws AmexException
    {
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        //Datos específicos para 1804
        messageType = "1804";
        processingCode = "000000";
        functionCode = FunctionCode.SYSTEM_AUDIT_CONTROL_ECHO_TEST.getFunctionCode();        
        messageReason = MessageReasonCode.value3.getMessageReasonCode();
        
        
        authRequestBean.setMessageTypeIdentifier(messageType);
        authRequestBean.setProcessingCode(processingCode);
        authRequestBean.setFunctionCode(functionCode);
        authRequestBean.setSystemTraceAuditNumber(transactionId);        
        authRequestBean.setMessageReasonCode(messageReason);
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss); 
        
        
        return sendRequestSimple(authRequestBean, authResponseBean);
    }
    

    private String sendRequestSimple(AuthorizationRequestBean authRequestBean,AuthorizationResponseBean authResponseBean) throws AmexException
    {
        String res = new String();
        
        AuthorizationService authorizationService  = new AuthorizationService();        
        System.out.println("AMEX:Init...");        
        
        try 
        {
            if(authResponseBean.getAuthErrorList()== null ||authResponseBean.getAuthErrorList().isEmpty() )
            {
                System.out.println("AMEX:getAuthErrorList is null or empty");        
            }
            else
            {
                Iterator<ErrorObject> errorList1 =authResponseBean.getAuthErrorList().iterator();
                while (errorList1.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList1.next();
                    System.out.println("AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription());        
                }
            }
                
            
            //authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);


            if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
            {
                System.out.println("AMEX:getAuthErrorList is null or empty");       

                try 
                { 
                    PropertyReader propertyReader=new PropertyReader();

                    propertyReader=propertyReader.setProxyDetailsFile("/authorization.properties");
                    //System.out.println("After Setting Property");

                  //Changes made on JUN06_2AM_MST . Calling validateAuthorizationRequest method to set the bean values in the request
                    authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);
                  //Changes made on JUN06_2AM_MST . For debugging the request parameters
                  //  System.out.println("AMEX:**Request:< "+authRequestBean.toString()+" >");

                  //Changes made on JUN06_2AM_MST . Call create and Send method only when there is no validation errors
                    if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
                    {
                        //System.out.println("Inside the if");
                        authResponseBean= authorizationService.createAndSendAuthorizationRequest(authRequestBean,propertyReader);
                        System.out.println("AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >");
                        System.out.println("AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >");
                        /** ANSWER **/
                    }

                     res+="******Request:\n"+authRequestBean;
                     res+="\n\n******Response:"+authResponseBean;
                     if ( authResponseBean != null && authResponseBean.getAuthErrorList() != null)
                     {
                        System.out.println("AMEX:******Response Errors:\n");
                        res+="\n\n******ERRORS*******\n";
                        Iterator errorlist=authResponseBean.getAuthErrorList().iterator();
                        while(errorlist.hasNext())
                        {
                            ErrorObject errorObject = (ErrorObject) errorlist.next();
                            System.out.println("AMEX:**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription());
                            res+="**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription();
                        }

                     }
                     else
                         res+="\n\n\n******AMEX ANSWER:\nRequest:<"+authResponseBean.getISOFormattedRequest()+">\n\nResponse:<"+authResponseBean.getISOFormattedResponse()+">";


                    authorizationService.flushRequestMemory(authRequestBean);

                    authorizationService.flushResponseMemory(authResponseBean);

                } 
                catch (Exception e) 
                {
                    System.out.println("AMEX:**ERROR Authorization: ->"+e.getMessage()+"\n\n*****************************\n");        
                    e.printStackTrace(System.out);
                }
            } 
 
            else
            {
                System.out.println("AMEX:**ERRORES getAuthErrorList is null");        
                Iterator<ErrorObject> errorList =authResponseBean.getAuthErrorList().iterator();
                while (errorList.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList.next();
                    System.out.println("AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription());        
                }
            }
        } 
        catch (Exception e) 
        {
            System.out.println("AMEX:ERROR: "+e.getMessage());
            System.err.println(e.getMessage());
        }        
        return res;

        
    }
    
    private String sendRequest() throws AmexException
    {
        String res = new String();
        System.out.println("AMEX:Init..");
        
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        //DataField 1
        authRequestBean.setMessageTypeIdentifier(messageType);
        //DataField 2
        authRequestBean.setPrimaryAccountNumber(new StringBuffer(primaryAccountNumber));
        //DataField 3
        authRequestBean.setProcessingCode(processingCode);
        //DataField 4
        authRequestBean.setAmountTransaction(amount);	
        //DataField 7
        authRequestBean.setTransmissionDateAndTime(yymmddhhmmss);
        //DataField 11 
        authRequestBean.setSystemTraceAuditNumber(transactionId);
        //DataField 12 
        authRequestBean.setLocalTransactionDateAndTime(yymmddhhmmss);	
        //DataField 13 No Aplicable
        /* authRequestBean.setCardEffectiveDate("1005"); */
        //DataField 14  
        authRequestBean.setCardExpirationDate(cardExpirationDate);
        //DataField 19
        authRequestBean.setMerchantLocationCountryCode(countryCode);
        //DataField 22
        authRequestBean.setPosDataCode(pos);
        //Datafield 24
        authRequestBean.setFunctionCode(functionCode);
        //Datafield 25
        authRequestBean.setMessageReasonCode(messageReason);
        //Datafield 26
        authRequestBean.setCardAcceptorBusinessCode(cardAcceptorBusiness);
        //Datafield 27
        authRequestBean.setApprovalCodeLength(approvalCodeLenght);
        //Datafield 37
        authRequestBean.setRetrievalReferenceNumber(retrievalReferenceNumber);
        //Datafield 41
        //authRequestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId);
        //Datafield 42
        authRequestBean.setCardAcceptorIdCode(cardAcceptorId);
        //Datafield 43
        
        // *** PENDIENTE ***
        CardAcceptorNameLocationBean cardAcceptNameLocationBean = new CardAcceptorNameLocationBean();
        cardAcceptNameLocationBean.setSubfield1Name(tppName);
        cardAcceptNameLocationBean.setSubfield1Street(tppStreet);
        cardAcceptNameLocationBean.setSubfield1City(tppCity);
        cardAcceptNameLocationBean.setSubfield2PostalCode(tppPostalCode);
        //Option 2 By using subfields - set the CardAcceptorNamelocation onto the AuthRequestObject 
        authRequestBean.setCardAcceptorNameLocation(cardAcceptNameLocationBean.populateCardAccpetorNameLocation(authResponseBean));
        
        AdditionalDataNationalBean additionalDataNationalBean = new AdditionalDataNationalBean();

        //**** For ITD -Start
        // Please do not set PRIMARY ID -- automatically handled by sdk
        //SECONDARY ID - Valid IDs include: IAC = Internet Airline Customer;Card Not Present (Mail-, Telephone- and Internet-Order) ITD;
        additionalDataNationalBean.setSecondaryId(itdSecondaryKey);
        //Please do not set CUSTOMER EMAIL ID (CE ID)-- automatically handled by sdk
        //CUSTOMER EMAIL
        additionalDataNationalBean.setCustomerEmail(itdCustomerEmail);
        //Please do not set  CUSTOMER HOSTNAME ID (CH ID) -- automatically handled by sdk
        //CUSTOMER HOSTNAME
        additionalDataNationalBean.setCustomerHostname(itdCustomerHostName);
        // Please do not set HTTP BROWSER TYPE ID (HBT ID)-- automatically handled by sdk
        //HTTP BROWSER TYPE
        additionalDataNationalBean.setHttpBrowserType(itdHttpBrowserType);
        //Please do not set  SHIP TO COUNTRY ID (STC ID)-- automatically handled by sdk
        //SHIP TO COUNTRY
        additionalDataNationalBean.setShipToCountry(itdShipToCountry);
        //Please do not set SHIPPING METHOD ID (SM ID)-- automatically handled by sdk
        //SHIPPING METHOD
        additionalDataNationalBean.setShippingMethod(itdShippingMethod);
        //Please do not set MERCHANT PRODUCT SKU ID (MPS ID)-- automatically handled by sdk
        //MERCHANT PRODUCT SKU
        additionalDataNationalBean.setMerchantProductSku(itdMerchantProductSku);
        //CUSTOMER IP
        additionalDataNationalBean.setCustomerIP(itdCustomerIP);
        //CUSTOMER ANI
        additionalDataNationalBean.setCustomerANI(itdCustomerANI);
        
        //CUSTOMER II DIGITS
//        additionalDataNationalBean.setCustomerInfoIdentifier(itdCustomerInfoIdentifier);
//        authRequestBean.setAdditionalDataNational(additionalDataNationalBean.populateAdditionalDataNational(authResponseBean));
        
        //Field 49
        authRequestBean.setTransactionCurrencyCode(currencyCode);	
        
        System.out.println("AMEX:End Parameres...");
        	    
        AuthorizationService authorizationService  =new AuthorizationService();
        
        System.out.println("AMEX:Authorization Service Init...");        
        
        try {
            if(authResponseBean.getAuthErrorList()== null ||authResponseBean.getAuthErrorList().isEmpty() )
            {
                //Que fish aqui...
                System.out.println("AMEX:OK -> getAuthErrorList null or empty");        
            }
            else
            {
                Iterator<ErrorObject> errorList1 =authResponseBean.getAuthErrorList().iterator();
                while (errorList1.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList1.next();
                    System.out.println("AMEX:**ERROR: Error AMEX->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription());        
                }
            }
                
            
            //authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);


            if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
            {            
                try 
                {	
                    PropertyReader propertyReader=new PropertyReader();
                                            
                    propertyReader=propertyReader.setProxyDetailsFile("/authorization.properties");
                                        
                    authResponseBean= authorizationService.createAndSendAuthorizationRequest(authRequestBean,propertyReader);
                    System.out.println("AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >");
                    System.out.println("AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >");
                    /** RESPUESTA **/
                     //System.out.println("Request:"+authRequestBean);
                    
                     res+="***Request:\n"+authRequestBean;
                     //System.out.println("******Response ActionCode:"+authResponseBean.getActionCode());
                     res+="\n\n***Response ActionCode:"+authResponseBean.getActionCode()+"\n";
                     res += "AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >\n";
                     res += "AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >\n";
                     
                     if ( authResponseBean != null && authResponseBean.getAuthErrorList() != null)
                     {
                        System.out.println("AMEX:***authResponseBean Errors:");
                        res+="\n\n******ERRORES*******";
                        Iterator errorlist=authResponseBean.getAuthErrorList().iterator();
                        while(errorlist.hasNext())
                        {
                            ErrorObject errorObject = (ErrorObject) errorlist.next();
                            System.out.println("AMEX:**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription());
                            res+="\n**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription();
                        }
                         
                     }
                     
                    
                    authorizationService.flushRequestMemory(authRequestBean);

                    authorizationService.flushResponseMemory(authResponseBean);

                } 
                catch (Exception e) 
                {
                    System.out.println("AMEX:**ERROR: createAndSendAuthorizationRequest ->"+e.getMessage()+"\n\n*****************************\n");        
                    e.printStackTrace(System.out);
                }
            } 
            else
            {
                System.out.println("AMEX:**ERRORES getAuthErrorList is null");        
                Iterator<ErrorObject> errorList =authResponseBean.getAuthErrorList().iterator();
                while (errorList.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList.next();
                    System.out.println("AMEX:**ERROR: Error AMEX->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription());        
                }
            }
        } 
        catch (Exception e) {
            System.out.println("AMEX:ERROR: "+e.getMessage());
            System.err.println(e.getMessage());
        }
	
        return res;
        
    }
    
    private boolean verifyCardNumber(String cardNumber)
    {
        boolean isValid= false;
        String s = cardNumber;
        char checkDigit=' ';
        char[] array;
        int odds = 0,evens = 0, total=0, iCheckDigit=0;
        
        String msg="";
        
        int i=0;
        
        //reverse
        StringBuffer sb = new StringBuffer(s);
        sb = sb.reverse();
        s = sb.toString();

        
        //To Array
        array = s.toCharArray();
        
        if(array.length==15)
        {
            //Get checkDigit
            checkDigit = array[0];
            iCheckDigit = Character.digit(checkDigit, 10);
            System.out.println("AMEX:checkDigit:"+checkDigit);
            
            for(int x=0; x<array.length;x++)
                msg+=array[x]+", ";
            
            System.out.println("AMEX:array:"+msg);
            //Odds
            for(i=1;i<array.length;i+=2)
            {
                int j = 0;
                j = Character.digit(array[i], 10);
                System.out.println("AMEX:Inicia Odds:"+j);
                j = j*2;
                if(j>9)
                {
                    char[] n = (j+"").toCharArray();
                    j = Character.digit(n[0], 10) + Character.digit(n[1], 10);
                }
                odds += j;
                System.out.println("AMEX:***** Odds Sum:"+odds);
            }
            
            //Even
            for(i=0;i<array.length;i+=2)
            {
                int j = 0;
                j = Character.digit(array[i], 10);
                System.out.println("AMEX:Even 1:"+j);
                evens += j;
                System.out.println("AMEX:***** Even Sum:"+evens);
            }
            
        }
        
        
        //Suma
        total = odds + evens;
        
        if(total%10==0 || total%10==iCheckDigit)
            isValid = true;
        else
            isValid = false;

        return isValid;
    }
    
    private void swapExpirationDate()
    {
        String par1 = "";
        String par2 = "";
                
        String anio = "";
        String mes = "";
        
        par1 = cardExpirationDate.substring(0, 2);
        par2 = cardExpirationDate.substring(2, 4);
        
        if(Integer.parseInt(par1)>12)
        {
            anio = par1;
            mes = par2;
        }
        
        if(Integer.parseInt(par2)>12)
        {
            mes = par1;
            anio = par2;
        }
        
        cardExpirationDate = anio + mes;
    }

    public String justTest()
    {
        String res = "";
        AuthorizationRequestBean authRequestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean authResponseBean = new AuthorizationResponseBean();
        
        authRequestBean.setMessageTypeIdentifier("1804");
        authRequestBean.setProcessingCode("000000");
        authRequestBean.setFunctionCode("831");
        authRequestBean.setSystemTraceAuditNumber("033600");
        authRequestBean.setLocalTransactionDateAndTime("120521033600"); 
        authRequestBean.setMessageReasonCode("8700");
        //authRequestBean.setCardAcceptorIdCode("9351358974");
        
        AuthorizationService authorizationService  = new AuthorizationService();        
        System.out.println("AMEX:Begin...");        
        
        try 
        {
            if(authResponseBean.getAuthErrorList()== null ||authResponseBean.getAuthErrorList().isEmpty() )
            {
                System.out.println("AMEX:getAuthErrorList is null or empty");        
            }
            else
            {
                Iterator<ErrorObject> errorList1 =authResponseBean.getAuthErrorList().iterator();
                while (errorList1.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList1.next();
                    System.out.println("AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription());        
                }
            }
                
            
            //authResponseBean=authorizationService.validateAuthorizationRequest(authRequestBean);


            if(authResponseBean.getAuthErrorList()== null || authResponseBean.getAuthErrorList().isEmpty())
            {
                System.out.println("AMEX:getAuthErrorList is null or empty");       

                try 
                { 
                    PropertyReader propertyReader=new PropertyReader();
                                            
                    propertyReader=propertyReader.setProxyDetailsFile("/authorization.properties");
                                        
                    authResponseBean= authorizationService.createAndSendAuthorizationRequest(authRequestBean,propertyReader);
                    System.out.println("AMEX:**Request:< "+authResponseBean.getISOFormattedRequest()+" >");
                    System.out.println("AMEX:**Response:< "+authResponseBean.getISOFormattedResponse()+" >");
                    /** ANSWER **/
                    
                     res+="******Request:\n"+authRequestBean;
                     res+="\n\n******Response:"+authResponseBean;
                     if ( authResponseBean != null && authResponseBean.getAuthErrorList() != null)
                     {
                        System.out.println("AMEX:******Response Errors:\n");
                        res+="\n\n******ERRORS*******\n";
                        Iterator errorlist=authResponseBean.getAuthErrorList().iterator();
                        while(errorlist.hasNext())
                        {
                            ErrorObject errorObject = (ErrorObject) errorlist.next();
                            System.out.println("AMEX:**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription());
                            res+="**ERR["+errorObject.getErrorCode()+"]"+errorObject.getErrorDescription();
                        }
                         
                     }
                     else
                         res+="\n\n\n******AMEX ANSWER:\nRequest:<"+authResponseBean.getISOFormattedRequest()+">\n\nResponse:<"+authResponseBean.getISOFormattedResponse()+">";
                     
                    
                    authorizationService.flushRequestMemory(authRequestBean);

                    authorizationService.flushResponseMemory(authResponseBean);

                } 
                catch (Exception e) 
                {
                    System.out.println("AMEX:**ERROR Authorization: ->"+e.getMessage()+"\n\n*****************************\n");        
                    e.printStackTrace(System.out);
                }
            } 
            else
            {
                System.out.println("AMEX:**ERRORES getAuthErrorList is null");        
                Iterator<ErrorObject> errorList =authResponseBean.getAuthErrorList().iterator();
                while (errorList.hasNext()) 
                {
                    ErrorObject errorObject = (ErrorObject) errorList.next();
                    System.out.println("AMEX:**ERROR: ->"+errorObject.getErrorCode()+":"+errorObject.getErrorDescription());        
                }
            }
        } 
        catch (Exception e) 
        {
            System.out.println("AMEX:ERROR: "+e.getMessage());
            System.err.println(e.getMessage());
        }        
        return res;
    }
    
    
    public static String getLocalDateTime(String format) {
            return now(format);
    }
    
    public static String now(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());
    }

    
    public static String padLeft(long source, char charToPad, int number)
    {
        return String.format("%"+number+"s", Long.toString(source)).replace(' ', charToPad);
    }
    private String setTransactionId(long i)
    {
        String s = "";
        s = padLeft(i, '0', 6);
        return s;
    }
    
    public void sample()
    {
    }
    
}
