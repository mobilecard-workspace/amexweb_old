<%-- 
    Document   : AmexExecution
    Created on : 8/06/2012, 10:16:26 AM
    Author     : ADDCEL13
--%>

<%@page import="com.addcel.amex.AmexData"%>
<%@page import="java.util.List"%>
<%@page import="com.addcel.amex.AmexResponse"%>
<%@page import="com.addcel.amex.TransactionControl"%>
<%@page import="com.addcel.amex.AmexTransaction"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%--<%@page contentType="text" pageEncoding="UTF-8"%>--%>

<%
try
{
    String action = request.getParameter("action");
    String processingCode = request.getParameter("processingCode");
    String tarjeta = request.getParameter("tarjeta");
    String expira = request.getParameter("expira");
    String fecha = request.getParameter("fecha");
    String monto = request.getParameter("monto");
    String cid = request.getParameter("cid");
    String email = request.getParameter("email");
    String cp = request.getParameter("cp");
    String nombres = request.getParameter("nombres");
    String apellidos = request.getParameter("apellidos");
    String direccion = request.getParameter("direccion");
    String telefono = request.getParameter("telefono");
    
    String service = request.getParameter("service");
    String type = request.getParameter("type");    
    
    String shipToCP = request.getParameter("stCp");
    String shipToDireccion = request.getParameter("stDireccion");
    String shipToNombres = request.getParameter("stNombres");
    String shipToApellidos = request.getParameter("stApellidos");
    String shipToTelefono = request.getParameter("stTelefono");
    
    String reversalId = request.getParameter("reversal");
    String reversalTipo = request.getParameter("tipo");
    
    String paymentType = request.getParameter("payment");
    String installations = request.getParameter("install");
    String dpp = request.getParameter("dpp");
    
    double fee = 0l;
    long reversalIdn = 0l;

    AmexResponse respuesta = null;
          
    if(monto!=null)
        fee = Double.parseDouble(monto);
    
    if(reversalId!=null)
        reversalIdn = Long.parseLong(reversalId);
    
    
    String s="";
    
    reversalTipo = (reversalTipo==null?"":reversalTipo);
    
    
    AmexTransaction amex = new AmexTransaction();
    if(action.equals("1"))
    {
        respuesta = amex.networkManagementRequest();
    }
    else if(action.equals("2"))
    {
        AmexData datos = new AmexData();
        datos.setProcessingCode(processingCode);
        datos.setPrimaryAccountNumber(tarjeta);
        datos.setCardExpirationDate(expira);
        datos.setNumberAmount(fee);
        datos.setCid(cid);
        datos.setBillingEmail(email);
        datos.setBillingCP(cp);
        datos.setBillingDireccion(direccion);
        datos.setBillingNombre(nombres);
        datos.setBillingApellido(apellidos);
        datos.setBillingTel(telefono);
        
        datos.setShipToCP(shipToCP);
        datos.setShipToDireccion(shipToDireccion);
        datos.setShipToNombre(shipToNombres);
        datos.setShipToApellido(shipToApellidos);
        datos.setShipToTel(shipToTelefono);
        
        datos.setServiceIdentifier(service);
        datos.setTypeIdentifier(type);
        datos.setPaymentType(paymentType);
        datos.setInstallations(installations);
        datos.setDpp(dpp);

        
        //respuesta = amex.cardAuthorizationRequestAAV(tarjeta, expira, fee, cid, email, cp, direccion, nombres, apellidos, telefono);
        respuesta = amex.cardAuthorizationRequestAAV(datos);
    }
    else if(action.equals("3"))
    {
        respuesta = amex.cardReversalAdviceRequest(reversalIdn, reversalTipo);
    }
    
    s = "{[transaction:'"+respuesta.transactionId+"'][code:'"+respuesta.codigo+"'][dsc:"+respuesta.mensaje+"]";
    if(respuesta.hasErrors())
    {
        List errores = respuesta.getErrores();
        for(int i=0; i<errores.size();i++)
        {
            AmexResponse error = (AmexResponse)errores.get(i);
            s +="[error:"+error.codigo+"][errorDsc:'"+error.mensaje+"']";
        }
    }
    s+="}";
    response.getWriter().println(s);
}
catch(Exception e)
{
    e.printStackTrace(response.getWriter());
}

%>
